public class AccountManagerController {

    // deployment testing POC CI CD
    public List<Account> accList{get;set;}
    public String searchinput{get;set;}
    public List<Contact> conList {get;set;}
	public AccountManagerController() {
	}

    public PageReference SearchAccount(){
        accList = New List<account>();
        system.debug(searchinput.length());
        if(searchinput.length() > 1){
            String searchStr1 = '*'+searchinput+'*';
            String searchQuery = 'FIND \'' + searchStr1 + '\' IN Name Fields RETURNING  Account (Name)';
            List<List <sObject>> searchList = search.query(searchQuery);
            accList = ((List<Account>)searchList[0]);
            system.debug(accList);
            if(accList.size() == 0){
                apexPages.addmessage(new apexpages.message(apexpages.severity.Error,
                'Sory, no results returned with matching string..'));
                return null;
            }
        }
        else{
            apexPages.addmessage(new apexpages.message(apexpages.severity.Error,
                                        'Please enter at least two characters..'));
            return null;
        }
        return null;
    }

    public PageReference ContactLists()
    {
    if(ApexPages.currentPage().getParameters().get('id') != null)
      conList = [SELECT
                    id,Name,Phone,Email
                FROM
                    contact
                WHERE
                    accountId =: ApexPages.currentPage().getParameters().get('id')];
     return null;
    }

}