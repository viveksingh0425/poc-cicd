/**************************************************************************************************
 * ControllerClass : AccountSearchController
 * Created By : Vivek Singh
 --------------------------------------------------------------------------------------------------
 * Description : Standard Controller which Return Required data on VF Page Using Account Controller.
 * Test Class : AccountSearchControllerTest
 * VFPage : AccountSearchPage
 --------------------------------------------------------------------------------------------------
 * Version History:
 * Version  Developer Name    Date          Detail Features
 * 1.0      Vivek Singh     29/07/2018    Initial Development
 **************************************************************************************************/

public with sharing class AccountSearchController{
    
    /**
     * Method will call Standard Controller Account
     *
     * @Param NULL
     *
     * @return Nothing
     */
    public AccountSearchController(ApexPages.StandardController controller) 
    {
         numRender = false;
        listRender = false;
    }
    
    public String accSearch {get;set;}
    public List<Account> lstAcc {get;set;}
    public String accRecord {get;set;}
    public Integer numRec {get;set;}
    public Boolean numRender {get;set;}
    public Boolean listRender {get;set;}
    
    /**
     * Method will implement  Search Functionality Based on input Field of VF Page
     *
     * @Param NULL
     *
     * @return Nothing
     */
    public void doSearch() {
        numRender = true;
        listRender = true;
        String searchStr = '%' + accSearch + '%';
        lstAcc = [SELECT Name FROM Account WHERE Name LIKE :searchStr];
        numRec = lstAcc.size();
    }
    
    /**
     * Method will Clear All the Data From the VF Page
     *
     * @Param NULL
     *
     * @return Nothing
     */
    public void clear() {
        numRender = false;
        listRender = false;
    }
     /**
     * Method will Call in the context of Vf page
     *
     * @Param NULL
     *
     * @return AccountRecord
     */
    public Pagereference accObj() {
        Pagereference pg = new Pagereference('/' + accRecord);
        return pg;
    }
}