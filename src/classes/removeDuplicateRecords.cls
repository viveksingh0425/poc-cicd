global class removeDuplicateRecords implements Database.Batchable<SObject> , Database.Stateful {

    global Set<String> emailstring;
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([Select Email from Lead where Email != null]);
    }

    global void execute(Database.BatchableContext BC , List<Lead> scope){
        Set<String> emailstring = new Set<String>();
		List<Lead> duplicatelist = new List<Lead>();
        Map<String,Lead> EmailBookmap = new Map<String,Lead>();
        for(Lead ld : scope){
			if(!EmailBookmap.containsKey(ld.Email)){
				EmailBookmap.put(ld.Email , ld);
			}
			else{
				duplicatelist.add(ld);
			}
        }

        system.debug(duplicatelist);
		if(duplicatelist.size() > 0){
			delete duplicatelist;
		}
    }
    global void finish(Database.BatchableContext BC){
    }

}