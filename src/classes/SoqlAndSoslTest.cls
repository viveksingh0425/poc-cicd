@isTest
public with sharing class SoqlAndSoslTest {

    // Assgn 1: Test All the Contact Record Fetched
    Static testMethod void AllContactRecordTest(){
        TestDataFactory.createTestContactRecords(4);
        List<Contact> conrecord = SoqlAndSosl.AllContactRecord();
        System.assertEquals(4,conrecord.Size());
    }
    //Assgn 2: Test All Active Users
    Static testMethod void  allActiveUsertest(){
        TestDataFactory.createTestUserRecords(5);
        Map<Id, User> userrecord = SoqlAndSosl.All_Active_User_test();
        System.debug(userrecord.keySet().size()); //6-Active Users are Allredy in the org
        System.assertEquals(11,userrecord.keySet().size());
    }
    //Assgn 3: MapStructure
    //Maptype1 Map<String,Id> On Account Record
    Static testMethod void  Maptype1Test(){
        //public static void createTestAccountRecords(Integer numAcctK,Integer numAcctsM)
        TestDataFactory.createTestAccountRecords(4,4);
        Map<String,Id> acrecord = SoqlAndSosl.MAP_Structure1();
        System.debug(acrecord.keySet().size()); //6-Active Users are Allredy in the org
        System.assertEquals(8,acrecord.keySet().size());
    }
    //Maptype2 Map<Id,Account> On Account Record
    Static testMethod void  Maptype2Test(){
         //public static void createTestAccountRecords(Integer numAcctK,Integer numAcctsM)
        TestDataFactory.createTestAccountRecords(3,4);
        Map<Id,Account> acrecord = SoqlAndSosl.MAP_Structure2();
        System.debug(acrecord.keySet().size()); //6-Active Users are Allredy in the org
        System.assertEquals(7,acrecord.keySet().size());
    }
    // Assgn 4: Enrollment year Assignment
    // A: Account record where Enrollment_Year__c = 2013 & 2014
    Static testMethod void  EnrAccountRecordTestA(){
        //createAccountEnrRecords(Integer en_yr2010,Integer en_yr2013,Integer en_yr2014)
        TestDataFactory.createAccountEnrRecords(2,3,4);
        List<Account> acrecord = SoqlAndSosl.AccountRecordYearA();
        System.debug(acrecord.size()); //6-Active Users are Allredy in the org
        System.assertEquals(7,acrecord.size());
    }
    // B: Account record where Enrollment_Year__c = 2010
    Static testMethod void  EnrAccountRecordTestB(){
        //createAccountEnrRecords(Integer en_yr2010,Integer en_yr2013,Integer en_yr2014)
        TestDataFactory.createAccountEnrRecords(2,3,4);
        List<Account> acrecord = SoqlAndSosl.AccountRecordYearB();
        System.debug(acrecord.size()); //6-Active Users are Allredy in the org
        System.assertEquals(2,acrecord.size());
    }
    //Assgn 5 : Test all the fetched account record whos Billingcity not Kerala and Maharashtra
   Static testMethod void AccountRecordBillingcityTest(){
        //  TestDataFactory.createTestAccountRecords(WithoutKer,WithoutMah);
        TestDataFactory.createTestAccountRecords(1,3);
        List<Account> acrecord = SoqlAndSosl.AccountRecordBillingcity();
        System.debug(acrecord.size());
        System.assertEquals(4,acrecord.size());
    }
    // Assgn 6 : testOpertunityRecord
    Static testMethod void opportunityRecordsTest(){
        //  public static void createTestOpertunityRecords(Integer numOperrecord)
        TestDataFactory.createTestOpertunityRecords(5);
        List<opportunity> optrecord = SoqlAndSosl.OpportunityRecord();
        System.debug(optrecord.size());
        System.assertEquals(4,optrecord.size());
    }
    
    //Aggrigate Function testing
    //Assgn 1: Test Aggrigate Function query Closed date and Sum(Ammount)
    Static testMethod void AllOptRecordTest(){
        TestDataFactory.createTestOpertunityRecords(3);
        Opportunity a = new Opportunity(Name='TestA',
                                            Amount=12300,
                                            CloseDate = Date.parse('8/27/2015'),
										 	StageName ='Closed Lost');
        insert a;
        Map<Object,Object> optrecord = SoqlAndSosl.OpertunityRecord_Groupby();
        System.debug('testing'+optrecord.keySet().Size());
        System.assertEquals(2,optrecord.keySet().Size());
    }   
    //Assgn 2: Find total number of distinct Lead Record on the basis of leadSource
    Static testMethod void distinctLeadRecordTest(){
        TestDataFactory.createTestLeadRecords();
        List<AggregateResult> lstrecord = SoqlAndSosl.printLeadSource();
        System.debug(lstrecord.Size());
        System.assertEquals(3,lstrecord.Size());
    }
	
    //Assgn 3: Test Lead Record (nestedMap)
    Static testMethod void AllLeadRecordTest(){
        TestDataFactory.createTestLeadRecords();
        map<String,Map<String,Integer>> leadrecord = SoqlAndSosl.Nexted_MAP_Record();
        System.assertEquals(leadrecord.keySet().Size(),2);
    }
    
    //Child Parent Relationship
    //Assgn 1: 
    Static testMethod void lstContactRecMediaTest(){
       // createTestAccountAndContactRecords(Integer numAccts, Integer numContactsPerAcct)
        TestDataFactory.createTestAccountAndContactRecords(2,5);
       List<Contact> leadrecord = SoqlAndSosl.lstContactRecMedia();
        System.debug(leadrecord.Size());
        System.assertEquals(10,leadrecord.Size());
    }
    //Assgn 2:
   /* Static testMethod void lstCustomeRecCTest(){
       // createCustomeTestRecords(Integer numCustomeRec)
        TestDataFactory.createCustomeTestRecords(5);
        List<c__c> crecord = SoqlAndSosl.lstCustomRecordC();
        System.debug(crecord.Size());
        System.assertEquals(15,crecord.Size());
    } */
    //Parent Child Relationship
   // Assgn 1:
   Static testMethod void ac_ConJohnRecordTest(){
       // createTestAccountAndContactRecords(Integer numAccts, Integer numContactsPerAcct) 
			Account a = new Account(Name='John');
        	insert a;
       		Contact con = new Contact(firstname='Johnrec',
                                     lastname='john',
                                     AccountId=a.Id);
        	insert con;
       		TestDataFactory.createTestAccountAndContactRecords(1,2);
            Map<String,String> acoptrecord = SoqlAndSosl.ac_ConJohnRecord();
            System.debug(acoptrecord);
            System.debug(acoptrecord.keySet().Size());
            System.assertEquals(2,acoptrecord.keySet().Size());
    }
   //Assgn 2:
   Static testMethod void ac_OptCloseWonRecordTest(){
       // createTestAccountAndOperRecords(Integer numAccts, Integer numOperPerAcct)
        TestDataFactory.createTestAccountAndOperRecords(2,2);
       	Map<String,String> acoptrecord = SoqlAndSosl.ac_OptCloseWonRecord();
        System.debug(acoptrecord.keySet().Size());
        System.assertEquals(2,acoptrecord.keySet().Size());
    }
    
    //DML 2
    Static testMethod void acinactiveAccountTest(){
       // createTestAccountRecords(Integer numAcctK,Integer numAcctsM)
       Account testAcc  = new Account ();          
		testAcc.Name = 'TestAccount';
		insert testAcc;
			
		Test.startTest();
			SoqlAndSosl.acinactiveAccount();
			Account deletedAccount = [SELECT Id, IsDeleted FROM Account WHERE Id = :testAcc.Id ALL ROWS];
			System.assertEquals(deletedAccount.IsDeleted, true);
		Test.stopTest();
       
    }
}