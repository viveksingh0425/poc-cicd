/**************************************************************************************************
* Class : WeatherControllerTest
* Created By : Vivek Singh
--------------------------------------------------------------------------------------------------
* Description : Test class for WeatherController class.
--------------------------------------------------------------------------------------------------
* Version History:
* Version  Developer Name    Date          Detail Features
* 1.0      Vivek Singh       04/01/2019    Initial Development
**************************************************************************************************/

@isTest
public class WeatherControllerTest {
	
    static testMethod void saveRecordTest() {
        
        Weather_History__c objWeatherHistory = new Weather_History__c();
        objWeatherHistory.City_Name__c = 'Mumbai';
        objWeatherHistory.Start_Date__c = datetime.newInstance(2019, 1, 14, 12, 30, 0);
        objWeatherHistory.End_Date__c = datetime.newInstance(2019, 1, 16, 13, 30, 0);
        insert objWeatherHistory;
        
        PageReference pageRef = Page.WeatherAPPVFPage; 
        Test.setCurrentPage(pageRef);
        
        Test.setMock(HttpCalloutMock.class, new HttpMockWeatherCallOut());
        
        ApexPages.StandardController sc = new ApexPages.StandardController(objWeatherHistory);
        WeatherController objWeatherController = new WeatherController(sc);
        
        objWeatherController.weatherdate = String.valueOf(objWeatherHistory.Start_Date__c);
        objWeatherController.temp = '29';
        objWeatherController.pressure = '1029';
        objWeatherController.humidity = '38';
		objWeatherController.temp_min = '27'        ;
        objWeatherController.temp_max = '32';
        WeatherController.WeatherWrapper objWeatherWrapper = new WeatherController.WeatherWrapper(objWeatherController.weatherdate,
                                                                                                 objWeatherController.temp,
                                                                                                 objWeatherController.pressure,
                                                                                                 objWeatherController.humidity,
                                                                                                 objWeatherController.temp_min,
                                                                                                 objWeatherController.temp_max);
        objWeatherController.listOfWeatherWraper.add(objWeatherWrapper);
        
        objWeatherController.saveRecord();
        objWeatherController.exportToPdf();
    }
}