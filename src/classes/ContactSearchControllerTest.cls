/**************************************************************************************************
 * Test Class : ContactSearchControllerTest
 * Created By : Vivek Singh
 --------------------------------------------------------------------------------------------------
 * Description : Test Class for AccountSearchPage & AccountSearchController
 * ControllerClass : ContactSearchController
 * VFPage : AlphabeticSearchPage
 --------------------------------------------------------------------------------------------------
 * Version History:
 * Version  Developer Name    Date          Detail Features
 * 1.0      Vivek Singh     29/07/2018    Initial Development
 **************************************************************************************************/

@isTest
public class ContactSearchControllerTest{
    
     /**
     * Test Method will implement NULL Testing on AlphabeticSearchPage
     *
     * @Param NULL
     *
     * @return Nothing
     */
    static testMethod void alphabeticalNullTest() {
        test.startTest();
        ContactSearchController alphaObj = new ContactSearchController();
        alphaObj.display();
        test.stopTest();
    }
    
     /**
     * Test Method will implement Positive Testing on AlphabeticSearchPage
     *
     * @Param NULL
     *
     * @return Nothing
     */
    static testMethod void alphabeticalPositiveTest() {
        test.startTest();
        Contact con = new Contact();
        con.lastname = 'anju';
        insert con;
        ContactSearchController alphaObj = new ContactSearchController();
        alphaObj.display();
        test.stopTest();
    }
    
     /**
     * Test Method will implement Negative Testing on AlphabeticSearchPage
     *
     * @Param NULL
     *
     * @return Nothing
     */
    static testMethod void alphabeticalNegativeTest() {
        test.startTest();
        Contact con = new Contact();
        con.lastname = 'anju';
        insert con;
        ContactSearchController alphaObj = new ContactSearchController();
        system.assert(alphaObj.SearchVar != 'null' || alphaObj.SearchVar != 'anju');

        alphaObj.display();
        
        test.stopTest();
        
    }
}