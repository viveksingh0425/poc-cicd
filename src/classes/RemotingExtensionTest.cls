/**************************************************************************************************
 * Test Class : RemotingExtensionTest
 * Created By : Vivek Singh
 --------------------------------------------------------------------------------------------------
 * Description : Test Class for Remoting Action & Deleting Contact
 * ControllerClass : RemotingExtension
 * VFPage : RemotingPage
 --------------------------------------------------------------------------------------------------
 * Version History:
 * Version  Developer Name    Date          Detail Features
 * 1.0      Vivek Singh     29/07/2018    Initial Development
 **************************************************************************************************/

@istest()
public class RemotingExtensionTest
{
     /**
     * Method will Test Delete Functionality (Positive Testing)
     *
     * @Param NULL
     *
     * @return Nothing
     */
    private static testmethod void DeleteContactPositiveTesting()
    {
        contact obj = new contact();
        obj.LastName = 'anju';
        obj.Email ='anju@gmail.com';
        obj.Phone = '83898';
        insert obj;
        
        PageReference pageRef = Page.RemotingPage;
        Test.setCurrentPage(pageRef);
        
        pageRef.getParameters().put('lastname','anju');
        pageRef.getParameters().put('Email','anju@gmail.com');
        pageRef.getParameters().put('Phone','83898');
        ApexPages.StandardController sc = new ApexPages.StandardController(obj);
        RemotingExtension testAccPlan = new RemotingExtension (sc);
        RemotingExtension.deleteContact('00B6F00000CxXBo');
        
    }
    
    /**
     * Method will Test Delete Functionality (Negative Testing)
     *
     * @Param NULL
     *
     * @return Nothing
     */
    private static testmethod void DeleteContactNegativeTesting()
    {
        contact obj = new contact();
        obj.LastName = 'anju';
        obj.Email ='anju@gmail.com';
        obj.Phone = '83898';
        PageReference pageRef = Page.RemotingPage;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('lastname','anju');
        pageRef.getParameters().put('Email','anju@gmail.com');
        pageRef.getParameters().put('Phone','83898');
        ApexPages.StandardController sc = new ApexPages.StandardController(obj);
        RemotingExtension testAccPlan = new RemotingExtension (sc);
        RemotingExtension.deleteContact('00B6F00000CxXBo');
        
    }
    
    /**
     * Method will Test Delete Functionality (NULL Testing)
     *
     * @Param NULL
     *
     * @return Nothing
     */
    private static testmethod void DeleteContactNullTesting()
    {
        contact obj = new contact();
        obj.LastName = '';
        obj.Email ='';
        obj.Phone = '';
        PageReference pageRef = Page.RemotingPage;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('lastname','');
        pageRef.getParameters().put('Email','');
        pageRef.getParameters().put('Phone','');
        ApexPages.StandardController sc = new ApexPages.StandardController(obj);
        RemotingExtension testAccPlan = new RemotingExtension (sc);
        RemotingExtension.deleteContact('00B6F00000CxXBo');
        
    }
}