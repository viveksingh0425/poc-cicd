/****************************************************************************************
 * Class : Insert_N_Records
 * Created By : Vivek Singh
 -------------------------------------------------------------------------------------------
 * Description : Create a method to insert Account record. Accept number of accounts to
				 be inserted using method parameters and insert it using only one DML
				 statement
 -------------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        13/07/2018       Initial Apex Learning
 ***************************************************************************************/

public with sharing class Insert_N_Account {
	public Insert_N_Account(Integer n) {

		try
		{
			List<Account> list_acc_insert = new List<Account>();
			for (Integer i=0;i<=n;i++){
				Account acc_insert = new Account(Name = 'vivek'+i);
				list_acc_insert.add(acc_insert);
			}
			System.debug(list_acc_insert);
			insert list_acc_insert;
		}catch(Exception e){
			System.debug('Error'+e);
		}

	}
}