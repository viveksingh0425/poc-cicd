/**************************************************************************************************
 * Class : CreateEventRecordsBatchJSON
 * Created By : Vivek Singh
 --------------------------------------------------------------------------------------------------
 * Description : RecordCreationEngine using JSON Parsing and Custome Setting.
 * Test Class : CreateEventRecordsBatchJSONTest
 * Scheduler Class : CreateEventRecordsBatchJSONScheduler
 --------------------------------------------------------------------------------------------------
 * Version History:
 * Version  Developer Name    Date          Detail Features
 * 1.0      Vivek Singh     08/03/2018    Initial Development
 **************************************************************************************************/
global class CreateEventRecordsBatchJSON implements Database.Batchable<sObject> {
    //Wrapper class of list of sobjects and their corrosponding
    public class sObjEventWrapper {
        List<sObject> lstObjs = new List<sObject>();
        List<JSON_EVENT__c> lstEvents = new List<JSON_EVENT__c>();
    }

    List<JSON_EVENT__c> updatedEvents = new List<JSON_EVENT__c>();

   String query = 'SELECT Error_Message__c,Id,JSON__c,RecordID__c,Status__c'+
                        'FROM JSON_EVENT__c WHERE Status__c = \'New\'';
    //Start method that fetches all the events.
  global Database.QueryLocator start(Database.BatchableContext bc) {
    return Database.getQueryLocator(query);
  }

    //Execute Method
     global void execute(Database.BatchableContext bc, List<JSON_EVENT__c> scope) {
        System.debug(scope);
        Map<String, Schema.SObjectType> globalSchema = Schema.getGlobalDescribe();
        Map<String, sObjEventWrapper> objTypeMap = new Map<String, sObjEventWrapper>();
        for (JSON_EVENT__c eveObj : scope) {
            try{
                String jsonInput= eveObj.JSON__c;
                Map<String, Object> jsonMap = (Map<String, Object>)JSON.deserializeUntyped(jsonInput);
                Map<String,Object> fieldMap = (Map<String, Object>)jsonMap.get('JSON');
                String objType = (String)jsonMap.get('Object Name');

                sObject sobj = globalSchema.get(objType).newSObject();
                for (String str: fieldMap.keySet()) {
                    sobj.put(str,fieldMap.get(str));
                }

                if(!objTypeMap.containsKey(objType)) {
                    sObjEventWrapper wrapper = new sObjEventWrapper();
                    wrapper.lstObjs.add(sObj);
                    wrapper.lstEvents.add(eveObj);
                    objTypeMap.put(objType,wrapper);
                }else {
                    objTypeMap.get(objType).lstObjs.add(sObj);
                    objTypeMap.get(objType).lstEvents.add(eveObj);
                }

            }catch(Exception ex){
                eveObj.Error_Message__c = ' Exception : ' +  ex.getMessage();
                eveObj.Status__c = 'Error';
                updatedEvents.add(eveObj);
            }
        }

        for (String objType : objTypeMap.keySet()) {
            List<sObject> lstObjs = objTypeMap.get(objType).lstObjs;
            List<JSON_EVENT__c> lstEvents = objTypeMap.get(objType).lstEvents;
            //DML to insert the sObject of single type.
            List<Database.SaveResult> lstSaveResult = Database.insert(lstObjs,false);

            for(Integer i = 0; i < lstSaveResult.size(); i++) {
                if(lstSaveResult[i].isSuccess()) {
                    lstEvents[i].Status__c = 'Complete';
                    lstEvents[i].RecordID__c= lstSaveResult[i].getId();
                }else {
                    lstEvents[i].Error_Message__c = 'DML Operation Failed ';
                    lstEvents[i].Status__c = 'Error';
                    for(Database.Error err : lstSaveResult[i].getErrors()) {
                        scope[i].Error_Message__c +='\n\n***\n following is the error.\n' +
                            err.getStatusCode() + ' : ' + err.getMessage() +
                            '\n Error On the Field: ' + err.getFields();
                    }
                }
                updatedEvents.add(lstEvents[i]);
            }
        }
        update updatedEvents;
  }
  global void finish(Database.BatchableContext bc) {
  }
}