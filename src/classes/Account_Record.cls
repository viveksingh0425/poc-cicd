/****************************************************************************************
 * Class : Account_Record
 * Created By : Vivek Singh
 ----------------------------------------------------------------------------------------
 * Description : Fetching Account record and Display using different loops
 ----------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        13/07/2018       Initial Apex Learning
 ***************************************************************************************/

public with sharing class Account_Record {

	public static void Account_Record() {

		try
		{
			List<Account> acount_obj =[SELECT
			ID,
			Name
			FROM
			Account
			];
			Integer length = acount_obj.Size();
			System.debug(length);
			for(Integer i = 0; i<=acount_obj.Size(); i++)
			{
				System.debug('Printing Using For LOOP'+acount_obj);
			}
			for(Account ac : acount_obj)
			{
				System.debug('Printing Using Advance For LOOP'+acount_obj);
			}
			while(length>0)
			{
				System.debug('Printing Using While LOOP'+acount_obj);
				length--;
			}
		}catch(DMLException ex){
			System.debug('Error:'+ex);
		}

	}
}