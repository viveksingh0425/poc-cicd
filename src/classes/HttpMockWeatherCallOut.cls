@isTest
global class HttpMockWeatherCallOut implements HttpCalloutMock {
    
    global HTTPResponse respond(HTTPRequest request) {
 
        // Create a fake response
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"access_token":"00D6F000002TnDy","instance_url":"http://api.openweathermap.org/data/2.5","id":"https://login.salesforce.com/id/00D6F000002TnDyUAK/0056F00000ABGY8QAP","token_type":"Bearer","issued_at":"1537176354111","signature":"fvZ5Z/qlI9PHlHKYkJhNeaPqgxLz7wZGYhpVBsC5CRo="}'+'[{"q":"Mumbai"}]');
        response.setStatusCode(200);
        response.setStatus('OK');
        return response; 
    }
}