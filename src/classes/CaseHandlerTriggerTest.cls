/***************************************************************************************************
 * Class : Casehandlertriggertest
 * Created By : Vivek Singh
 ---------------------------------------------------------------------------------------------------
 * Description :Trigger for Updating Cases that come in from the Web, Email or Phone
                Use Case – we would like to use the email address of the incoming case to see if we
                can associate the correct account AND/OR contact to populate the Account and Contact
                Fields.
                When a new case is created and the Case.Origin field is set to “Phone” or “Email” or
                “Web” take the
                Case.Email__c field and look up to find a match in the following account field –
                Account.Email_Address__c(Custome Field) And Conatct field - Contact.Email
 ---------------------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        19/07/2018       Initial Apex Trigger Learning
 ***************************************************************************************/

@isTest
public with sharing class CaseHandlerTriggerTest {
	public static testMethod void CaseHandlerTriggerTest() {

        testDataFactoryTrigger testdata = new testDataFactoryTrigger();
        //createAcRecPositive(Integer numAccts)
        testdata.createAcRecPositive(2); //Possitive test Account record
        //createAcRecNegative(Integer numAccts)
        testdata.createAcRecNegative(4); //Negative test Account record
        //createConRecPositive(Integer numAccts)
        testdata.createConRecPositive(2); //Possitive test Contact record
        //createConRecNegative(Integer numAccts)
        testdata.createConRecNegative(4); //Negative test Contact record
        //createcaseRecPositive(Integer numcase)
        testdata.createcaseRecPositive(2); //Possitive test case record
        //createcaseRecNegative(Integer numAccts)
        testdata.createcaseRecNegative(4); //Negative test case record
        List<Id> lstacid = new List<Id>();
        for(Account ac : [SELECT
                                id,Ac_email__c
                            FROM
                                Account]){
            lstacid.add(ac.id);
        }
        List<Id> lstconid = new List<Id>();
        for(Contact con : [SELECT
                                id,Email
                            FROM
                                Contact]){
            lstconid.add(con.id);
        }
        List<Case> lstcase = [SELECT
                                    id,AccountId,ContactId
                                FROM
                                    Case
                                WHERE
                                    AccountId IN: lstacid
                                AND
                                    ContactId IN: lstconid];
        System.debug(lstcase.size());
        System.assertEquals(2,lstcase.size()); // testpassed as positivecaserecord is 2;
	}
}