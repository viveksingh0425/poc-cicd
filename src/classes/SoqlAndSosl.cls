public with sharing class SoqlAndSosl {

    //Assgn 1:Query on all Contact records and add them to the List. Print that contents of this list.(testd)
    public static List<Contact> AllContactRecord() {

		//Creating Map for Account and associated Contact Records for that Particular ID
		List<Contact> Contact_record = [SELECT Id, name FROM Contact];
		System.debug(Contact_record);
        return Contact_record;
	}

    //Assgn 2 : All Active User (Tested)
    public static Map<Id, User> All_Active_User_test() {
        //Selecting Active User Details
        Map<Id, User> active_user_map = new Map<Id, User>([SELECT
                                                           Id,Name
                                                           FROM User
                                                           WHERE
                                                           IsActive = true]);
		System.debug(active_user_map);
        return active_user_map;
	}

    //Assgn 3 : Map Structure
    //Map Type1
	public static Map<String,Id> MAP_Structure1() {

		List<Account> Account_Record =[Select Id,Name from Account];
		Map<String,Id> Map_type = new Map<String,Id>();
		for(Account ac : Account_Record)
		{
			Map_type.put(ac.Name,ac.Id);
		}
		System.debug(Map_type);
        return Map_type;
	}
     //Map Type2
    public static Map<Id,Account> MAP_Structure2() {

		List<Account> Account_Record =[Select Id,Name from Account];
		Map<Id,Account> Map_type = new Map<Id,Account>();
		for(Account ac : Account_Record)
		{
			Map_type.put(ac.Id,ac);
		}
		System.debug(Map_type);
        return Map_type;
	}

    // Assgn 4: Enrollment year Assignment
    // A: Account record where Enrollment_Year__c = 2013 & 2014
    public static List<Account> AccountRecordYearA() {
		//INCLUDES Keyword is used to Check the Value of MultiSelect Picklist
		List<Account> account_record_multiple =[SELECT
												Id, Name
												FROM Account
												WHERE Account.Enrollment_Year__c  INCLUDES ('2013','2014')];
		System.debug(account_record_multiple);
        return account_record_multiple;
	}
    // B: Account record where Enrollment_Year__c = 2010
    public static List<Account> AccountRecordYearB() {
		//INCLUDES Keyword is used to Check the Value of MultiSelect Picklist
		List<Account> account_record_single =[SELECT
												Id, Name
												FROM Account
												WHERE Account.Enrollment_Year__c  INCLUDES ('2010')];
		System.debug(account_record_single);
        return account_record_single;
	}

    //Assgn 5 : Account Details where billing city is not Equal to maharashtra and Kerala
    public static List<Account> AccountRecordBillingcity() {
        List<Account> ac_record = [SELECT
                                    ID,Name
                                    FROM Account
                                    WHERE
                                    BillingState NOT IN ('Kerala','Maharashtra')
                                    ORDER BY BillingState DESC NULLS LAST
                                    LIMIT 10000];
                                    //Try with LIMIT 10 OFFSET 5 because data is not present
		System.debug(ac_record);
        return ac_record;
	}

    /* Assgn 6 : Write a SOQL query to display 100 opportunity records with amount greater than 10,000
                order by created date. Skip first 50 records and include records from recycle bin. */
    public static List<Opportunity> OpportunityRecord() {
        List<Opportunity> opt_record = [SELECT
                                        ID,Name
                                        FROM opportunity
                                        WHERE opportunity.amount > 10000
                                        ORDER BY createddate LIMIT 5 OFFSET 1 ALL ROWS];
                                        //Try with LIMIT 10 OFFSET 5 because data is not present
        System.debug(opt_record);
        return opt_record;
	}

    //Aggrigate function :

    //Assgn 1 : Opertunity groupBy using MAP-Object
    public static Map<Object,Object> OpertunityRecord_Groupby() {
        List<AggregateResult> Opt_Record = [SELECT
												CALENDAR_YEAR(CloseDate),SUM(Amount)
											FROM 
                                            	Opportunity
                                            WHERE StageName = 'Closed Lost'
											GROUP BY CloseDate];
		Map<Object,Object> map_yr_Opportunity = new Map<Object,Object>();
		for (AggregateResult ar : Opt_Record)  {
			map_yr_Opportunity.put(ar.get('expr0'),ar.get('expr1'));
			System.debug('CALENDAR_YEAR=' + ar.get('expr0') +'\tSUM of amount=' + ar.get('expr1'));
		}
		System.debug(map_yr_Opportunity);
        return map_yr_Opportunity;
    }

    // Assgn 2 : Find total number of distinct Lead Record on the basis of leadSource

    public static List<AggregateResult> printLeadSource(){
        List<AggregateResult> lstLead = [SELECT
                                         	LeadSource,
                                         	COUNT_DISTINCT(id) LeadCount
                                         FROM
                                         	Lead
                                         GROUP BY
                                         	LeadSource
                                         HAVING COUNT(id)<10];
        System.debug(lstLead);
        return lstLead;
    }
    // Assgn 3 : Nexted MAP
    public static map<String,Map<String,Integer>> Nexted_MAP_Record() {
        map<String,Map<String,Integer>> leadsource_status_map = new map<String,Map<String,Integer>>();
        List<AggregateResult> lead_record = [SELECT
                                              	Status, LeadSource, COUNT(Name)
                                              FROM 
                                             	Lead
                                              GROUP BY Status, LeadSource];
        for(AggregateResult ar : lead_record)
        {
			if(!leadsource_status_map.containskey(ar.get('Status').toString())) {
				leadsource_status_map.put(ar.get('Status').toString(),new Map<String,Integer>());
			}
			if(ar.get('LeadSource') != null){
				leadsource_status_map.get(ar.get('Status').toString()).put(ar.get('LeadSource').toString(),
                                            Integer.valueOf(ar.get('expr0')));
			}
			else {
				leadsource_status_map.get(ar.get('Status').toString()).put('Empty Lead Source',
                                            Integer.valueOf(ar.get('expr0')));
			}
		}
		system.debug('----leadsource_status_map----'+leadsource_status_map);
        return leadsource_status_map;
	}

    // Child to Parent Relationship
    // Assgn 1 :
    public static List<Contact> lstContactRecMedia(){
        List<contact> conRecord = [SELECT
                                   	id,Name,Account.Name
                                   FROM 
                                   	contact
                                   WHERE
                                   Account.Active__c = 'Yes' AND Account.Industry = 'Media'];
        System.debug(conRecord);
        return conRecord;
    }
     // Assgn 2 :
    public static List<c__c> lstCustomRecordC(){
        List<c__c> customObjC = [Select
                                        id,Name,C_A_Relation__r.Parent_Name__c,
                                        C_B_Relation__r.Parent_Name__c
                                    FROM
                                        c__c
                                    WHERE
                                        Name = 'john'];
        System.debug(customObjC);
        return customObjC;
    }

    // Parent to child Relationship
    // Assign 1 :
    public static Map<String,String> ac_ConJohnRecord(){
        Map<String,String> accContactRecord = new Map<String,String>();
        for(Account objAccount: [SELECT
                                    Account.name,(SELECT
                                                    Contact.name
                                                FROM
                                                    Account.Contacts
                                                WHERE
                                                    Contact.name LIKE '%john%')
                                FROM
                                    Account])
        {
            for(Contact objContact: objAccount.Contacts)
            {
                accContactRecord.put(objAccount.name,objContact.name);
                system.debug('====Contact info======'+objContact);
            }
        }
        system.debug('====after Record======'+accContactRecord);
        return accContactRecord;
    }
    // Assign 2 :
    public static Map<String,String> ac_OptCloseWonRecord(){
        Map<String,String> accOptRecord = new Map<String,String>();
        for(Account objAccount: [SELECT
    		                        Name,
    		                        (SELECT
                                        Opportunity.Name
                                    FROM
                                        Opportunities
    			                    WHERE
                                        Opportunity.StageName = 'Closed Won')
                                    FROM
                                        Account]){
            for(Opportunity objOpportunity: objAccount.Opportunities)
            {
                accOptRecord.put(objAccount.name,objOpportunity.name);
            }
        }
        system.debug('==== Record======'+accOptRecord);
        return accOptRecord;
    }
    // Assign 3:
    public static List<A__c> relationshipRecord(){
        List<A__c> lstA = [SELECT
                                A__c.name,(SELECT
                                                C_A_Relation__r.name,C_B_Relation__r.name From A__c.c__r
                                            WHERE
                                                C_A_Relation__r.name = 'John')
                            FROM
                                A__c];
        System.debug(lstA);
        return lstA;
    }

    // SOSL Query
    public static List<List<SObject>> SoslFindResult() {
		String str = 'test';
		List<List<SObject>> searchList = [FIND 'test*' IN ALL
		FIELDS RETURNING Contact(Name),Account(Name),Lead(Name),User(Name)];
		Contact[] cnList = ((List<Contact>)searchList[0]);
		Account[] acList = ((List<Account>)searchList[1]);
		Lead[] ldList = ((List<Lead>)searchList[2]);
		User[] userList = ((List<User>)searchList[3]);
		System.debug(cnList);
		System.debug(acList);
		System.debug(ldList);
		System.debug(userList);
        return searchList;
	}

    //DML Operation
    // Assgn 1 : check for the class Error_Logs
    // Assgn 2 : Delete all inactive records from last 90days
    // Actual Query SELECT Id FROM Account WHERE CreatedDate < LAST_90_DAYS , for testing CreatedDate = today
    public static List<Account> acinactiveAccount(){
        List<Account> lstAccount = [SELECT Id FROM Account WHERE CreatedDate = today];
        delete lstAccount;
        return lstAccount;
    }
    // Assgn 3 : point and click

}