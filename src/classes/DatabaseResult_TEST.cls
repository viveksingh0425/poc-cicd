/****************************************************************************************
 * Class : DatabaseResult_TEST
 * Created By : Vivek Singh
 --------------------------------------------------------------------------------------------------------
 * Description : Database.SaveResult[] class testing with success and failed results.
 --------------------------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        16/07/2018       Initial Apex Learning
 ***************************************************************************************/

public with sharing class DatabaseResult_TEST {
	public static void Database_Result() {

		// Create two accounts, one of which is missing a required field
		Account ac_testuser = new Account(
							Name='Jayesh',
							Phone='(022)155-1222',
							BillingCity='Aundh');
		Account ac_test = new Account(
							//Name='ramesh',
							Phone='(022)555-0212',
							BillingCity='Banaras');
		List<Account> list_ac = new List<Account>();
		list_ac.add(ac_testuser);
		list_ac.add(ac_test);
		Database.SaveResult[] srList = Database.insert(list_ac, false);
		// Iterate through each returned result
		for (Database.SaveResult sr : srList) {
			if (sr.isSuccess()) {
				// Operation was successful, so get the ID of the record that was processed
				System.debug('Successfully inserted account. Account ID: ' + sr.getId());
			}
			else {
				// Operation failed, so get all errors
				for(Database.Error err : sr.getErrors()) {
					System.debug('The following error has occurred.');
					System.debug(err.getStatusCode() + ': ' + err.getMessage());
					System.debug('Account fields that affected this error: ' + err.getFields());
				}
			}
		}
	}
}