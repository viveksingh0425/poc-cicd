/****************************************************************************************
 * Class : Replace
 * Created By : Vivek Singh
 ----------------------------------------------------------------------------------------
 * Description : Replace a char in word
 ----------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        12/07/2018       Initial Apex Learning
 ***************************************************************************************/

public with sharing class replacea {
	public static String replacea(String str,String str1,String word) {
		integer index = word.indexOf(str);
		while (index >= 0)
		{
    		System.debug(index);
			word = word.replace(str,str1);
    		index = word.indexOf(word, index + 1);
		}
		System.debug(word);
		return word;

	}
}

/* Calling

replacea.replacea('v','r','vivek');




*/