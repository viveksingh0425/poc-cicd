public with sharing class QueryBuilderVFController {
	public QueryBuilderVFController() {
	}

    public List<SelectOption> getobjNames()
    {
        List<Schema.SObjectType> GlobalSobject = Schema.getGlobalDescribe().Values();
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('--None','--None--'));
        for(Schema.SObjectType sobjectname : GlobalSobject)
        {
            options.add(new SelectOption(sobjectname.getDescribe().getName(),sobjectname.getDescribe().getName()));
        }
    return options;
    }
}