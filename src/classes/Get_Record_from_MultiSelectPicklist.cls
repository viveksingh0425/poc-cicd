/****************************************************************************************
 * Class : Get_Record_From_MultiSelectPicklist
 * Created By : Vivek Singh
 --------------------------------------------------------------------------------------------
 * Description :Create a multi-select picklist on Account object called as 'Enrollment Year' with values -
 				2010, 2011,	2012, 2013, 2014, 2015 and 2016.
				Get all account records where in selected 'Enrollment Year' is:
					a. 2010
					b. 2013 and 2014
 --------------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        16/07/2018       Initial Apex Learning
 ***************************************************************************************/

public with sharing class Get_Record_from_MultiSelectPicklist {
	public Get_Record_from_MultiSelectPicklist() {
		//INCLUDES Keyword is used to Check the Value of MultiSelect Picklist
		List<Account> account_record_multiple =[SELECT
												Id, Name
												FROM Account
												WHERE Account.Enrollment_Year__c  INCLUDES ('2013','2014')];
		List<Account> account_record_single = [SELECT
												Id, Name
												FROM Account
												WHERE Account.Enrollment_Year__c  INCLUDES ('2010')];
		System.debug(account_record_multiple);
		System.debug(account_record_single);
	}
}