public class Subclass extends SuperClass {
  public override void printName() {
        super.printName();
        System.debug('But you can call me ' + super.getFirstName());
    }
}

/*
 
Subclass obj = new Subclass();
obj.printName();

or

call p.constructor with parameters

*/