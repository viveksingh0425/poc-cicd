/****************************************************************************************
 * Class : Lead_Record_Set
 * Created By : Vivek Singh
 -------------------------------------------------------------------------------------------
 * Description : Query on all Lead records & add ID of record to the set and print that set.
 -------------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        13/07/2018       Initial Apex Learning
 ***************************************************************************************/


public with sharing class Lead_Records_Set {
	public Lead_Records_Set() {

		try
		{
			List<Lead> setlead = [SELECT ID FROM Lead];
			Set<Id> setID = new Set<Id>();
			for(Lead ld : setlead){
				setID.add(ld.ID);
			}
			System.debug(setID);
		}catch(Exception e){
			System.debug('Error:'+e);
		}

	}
}