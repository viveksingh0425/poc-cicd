/****************************************************************************************
 * Class : SOSL_FIND_TEST
 * Created By : Vivek Singh
 --------------------------------------------------------------------------------------------------------
 * Description :  Find the word 'test' in all name fields returning Contact, Account, Lead and User.
 --------------------------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        16/07/2018       Initial Apex Learning
 ***************************************************************************************/

public with sharing class SOSL_FIND_TEST {
	public SOSL_FIND_TEST() {

		List<List<SObject>> searchList = [FIND 'test*' IN ALL
		FIELDS RETURNING Contact(Name),Account(Name),Lead(Name),User(Name)];
		Contact[] cnList = ((List<Contact>)searchList[0]);
		Account[] acList = ((List<Account>)searchList[1]);
		Lead[] ldList = ((List<Lead>)searchList[2]);
		User[] userList = ((List<User>)searchList[3]);
		System.debug(cnList);
		System.debug(acList);
		System.debug(ldList);
		System.debug(userList);
	}
}