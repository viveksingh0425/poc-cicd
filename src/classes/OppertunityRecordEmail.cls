/**************************************************************************************************
 * Class : OppertunityBatch
 * Created By : Vivek Singh
 --------------------------------------------------------------------------------------------------
 * Description : Batch to send A email for oppertunity amount and  aggregate of it.
 * Test Class : OppertunityBatchTest
 --------------------------------------------------------------------------------------------------
 * Version History:
 * Version  Developer Name    Date          Detail Features
 * 1.0      Twinkle Panjabi   10/04/2017    Initial Development
 **************************************************************************************************/
global class OppertunityRecordEmail implements Database.batchable<SObject>{

     /**
     * Start the batch and generate the QueryLocator based on the queryString.

     */
    global Iterable<SObject> start(Database.BatchableContext bc) {
        return Database.getQueryLocator(
            'SELECT Name,'+
                '(SELECT email FROM Contacts),'+
                '(SELECT Name, Amount FROM Opportunities WHERE stagename = \'Closed Won\')'+
            'FROM Account'+
            'WHERE Id IN (SELECT AccountId FROM Opportunity where Amount != NULL)'
        );
    }

    /**
     * Execute the function for the current scope.
     */
    global void execute(Database.BatchableContext bc, List<Account> scope){
        // process each batch of records
        List<Account> accountRecord = new List<Account>();
        System.debug(scope);
       /* for (Account account : scope) {
            for (Contact contact : account.contacts) {
                contact.MailingStreet = account.BillingStreet;
                contact.MailingCity = account.BillingCity;
                contact.MailingState = account.BillingState;
                contact.MailingPostalCode = account.BillingPostalCode;
                // add contact to list to be updated
                contacts.add(contact);
                // increment the instance member counter
                recordsProcessed = recordsProcessed + 1;
            }
        }
        update contacts;*/
    }

    global void finish(Database.BatchableContext BC) {
     /* AsyncApexJob a = [Select Id, Status,ExtendedStatus,NumberOfErrors,     JobItemsProcessed,
        TotalJobItems, CreatedBy.Email
        from AsyncApexJob where Id =:BC.getJobId()];
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {a.CreatedBy.Email};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Match Merge Batch ' + a.Status);
            mail.setPlainTextBody('records processed ' + a.TotalJobItems +
           'with '+ a.NumberOfErrors + ' failures.');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); */
    }

}