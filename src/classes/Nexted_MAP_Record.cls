/****************************************************************************************
 * Class : Nexted_Map_Record
 * Created By : Vivek Singh
 --------------------------------------------------------------------------------------------
 * Description : Write a SOQL query to find total number of Lead records by status by Lead Source.
                Store this information in map and display the same.
                (Hint: map<string,map<string,integer>>)
 --------------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        16/07/2018       Initial Apex Learning
 ***************************************************************************************/

public with sharing class Nexted_MAP_Record {
	public map<String,Map<String,Integer>> Nexted_MAP_Record() {

		map<String,Map<String,Integer>> leadsource_status_map = new map<String,Map<String,Integer>>();
        List<AggregateResult> lead_record = [SELECT Status, LeadSource, COUNT(Name) FROM Lead GROUP BY Status, LeadSource];
        for(AggregateResult ar : lead_record)
        {
			if(!leadsource_status_map.containskey(ar.get('Status').toString())) {
				leadsource_status_map.put(ar.get('Status').toString(),new Map<String,Integer>());
			}
			if(ar.get('LeadSource') != null){
				leadsource_status_map.get(ar.get('Status').toString()).put(ar.get('LeadSource').toString(),Integer.valueOf(ar.get('expr0')));
			}
			else {
				leadsource_status_map.get(ar.get('Status').toString()).put('Empty Lead Source',Integer.valueOf(ar.get('expr0')));
			}
		}
		system.debug('----leadsource_status_map----'+leadsource_status_map);
        return leadsource_status_map;
	}
}