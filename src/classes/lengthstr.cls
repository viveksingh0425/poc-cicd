/****************************************************************************************
 * Class : Lengthstr
 * Created By : Vivek Singh
 ----------------------------------------------------------------------------------------
 * Description : Length of String without library function
 ----------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        12/07/2018       Initial Apex Learning
 ***************************************************************************************/

public with sharing class lengthstr {
	public static Integer lengthstr(String str) {
			integer res=0;
			List<String> myArray = str.split(''); //Spliting the String
			for(Integer i=0;i<myArray.size();i++)
			{
				res++;
			}
			System.debug(res);
			return res;
	}
}