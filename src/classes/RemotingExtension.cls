/**************************************************************************************************
 * ControllerClass : RemotingExtension
 * Created By : Vivek Singh
 --------------------------------------------------------------------------------------------------
 * Description : Create a new link on the contact row to delete the contact from the account.
                      On clicking the link, the contact should be deleted this time using VisualForce
                      Remoting.On successful deletion, an alert should be displayed on the page stating
                      “The contact has been deleted”.
 * Test Class : RemotingExtensionTest
 * VFPage : RemotingPage
 --------------------------------------------------------------------------------------------------
 * Version History:
 * Version  Developer Name    Date          Detail Features
 * 1.0      Vivek Singh     29/07/2018    Initial Development
 **************************************************************************************************/

/* public class RemotingExtension{

    public static List<Contact> lstContact{get; set;}
    public static List<Contact> listCon{get; set;}
    /**
     * Method will Call Standard Controller Contact
     *
     * @Param NULL
     *
     * @return Nothing
     */
  /*  public  RemotingExtension(ApexPages.StandardController controller){
        lstContact = [
                        SELECT
                            id,
                            Name,
                            Birthdate,
                            Email,
                            Phone
                        FROM
                            Contact
                    ];
    }

    /**
     * Method will Call RemoteAction
     *
     * @Param List<Contact>
     *
     * @return Nothing
     */
   /* @RemoteAction
    public static List<Contact> deleteContact(ID c_Id)
    {
        System.debug('c_Id-->' + c_Id);
        ID conId = c_Id;

        List<Contact> conList = [
                                    SELECT
                                        id,
                                        Name
                                    FROM
                                        Contact
                                    WHERE
                                        id = :conId
                                ];
        delete conList;
        return lstContact;
    }
}
*/

global with sharing class RemotingExtension {

public static List<Contact> lstContact{get; set;}
public static Contact listCon{get; set;}

public RemotingExtension(ApexPages.StandardController controller)
{
    listCon = new Contact();
    initContactList(); // initialize your list with data
}

public void initContactList(){ // #### CHANGE #### ***** method to get data list into your page variable*****
   lstContact = [
                    SELECT
                        id,
                        Name
                    FROM
                        Contact
                ];
}

@RemoteAction
global static List<Contact> deleteContact(ID c_Id) {

    Contact[] conn;
    conn = [
                SELECT
                    id,
                    Name
                FROM
                    Contact
                WHERE
                    id = :c_Id
            ];
    if(conn.size() > 0 || conn[0].Id != ''){
        delete conn;
    }
   List<Contact> lstContact = [
                                SELECT
                                    id,
                                    Name
                                FROM
                                    Contact
                            ];   // #### CHANGE #### ***** initialize the list Object *****

    return lstContact;

}}