public with sharing class CaseHelper {

    public static void casehelpermethod(Case[] caseobject)
    {

        List<String> emailAddresses = new List<String>();
        //For loop to add all the caseEmail into createdlist
        for (Case caseObj:caseobject) {
            if (caseObj.ContactId==null &&
                caseObj.Case_Email__c!='')
            {
                emailAddresses.add(caseObj.Case_Email__c);
                System.Debug('>>> the value of the supplied email is ' + caseObj.Case_Email__c);
            }
        }
        System.debug(emailAddresses);
        System.Debug('>>> the size of the email address object is ' + emailAddresses.size());
        //Select all the contact details whoes email is matched with case-email
        List<Contact> listContacts = [SELECT
                                    Id,Email
                                    FROM
                                    Contact
                                    WHERE
                                    Email in :emailAddresses];
        //Select all the Account details whoes email is matched with case-email
        List<Account> listAccount = [SELECT
                                    Id,Ac_email__c
                                    FROM
                                    Account
                                    WHERE
                                    Ac_email__c in :emailAddresses];
        Set<String> takenEmailsContacts = new Set<String>(); //unique matched conatact emails
        Set<String> takenEmailsAccount = new Set<String>();  //unique matched Account emails
        for (Account c: listAccount) {
            takenEmailsAccount.add(c.Ac_email__c);
        }
        for (Contact c: listContacts) {
            takenEmailsContacts.add(c.Email);
        }
        //Map having key as case-email and all the matched Account and contact SObject
        Map<String,List<Account>> emailToAccountMap= new Map<String,List<Account>>();
        Map<String,List<Contact>> emailToContactMap= new Map<String,List<Contact>>();
        List<Case> casesToUpdate = new List<Case>(); //list for updating case
        //Used to match the case email with account and contact
        for (Case caseObj:caseobject) {
            if (takenEmailsAccount.contains(caseObj.Case_Email__c) &&
                    takenEmailsContacts.contains(caseObj.Case_Email__c))
            {
                emailToAccountMap.put(caseObj.Case_Email__c,listAccount);
                emailToContactMap.put(caseObj.Case_Email__c,listContacts);
                casesToUpdate.add(caseObj);
            }
        }
        //case for populating account and contact ID with the case
        for (Case caseObj:casesToUpdate) {
            Account[] acAccount = emailToAccountMap.get(caseObj.Case_Email__c);
            System.debug(acAccount[0].Id);
            caseObj.AccountId = acAccount[0].Id;
            Contact[] conContact = emailToContactMap.get(caseObj.Case_Email__c);
            System.debug(conContact[0].Id);
            caseObj.ContactId = conContact[0].Id;
        }


    }
}