/**************************************************************************************************
 * Class : EmailopportunityRecord
 * Created By : Vivek Singh
 --------------------------------------------------------------------------------------------------
 * Description : Email Opportunity Record Amount to contact Email.
 * Test Class : EmailopportunityRecordTest
 * Scheduler Class : EmailopportunityRecordScheduler
 --------------------------------------------------------------------------------------------------
 * Version History:
 * Version  Developer Name    Date          Detail Features
 * 1.0      Vivek Singh     08/03/2018    Initial Development
 **************************************************************************************************/
     /**
     * Start the batch and generate the QueryLocator based on the queryString.
        Select id,Email,AccountId,Account.Name from contact where email != NULL

        SELECT Amount,Id,Name,AccountId From opportunity where Amount != Null And StageName = 'Closed Won'

        SELECT id,(SELECT email FROM Contacts where email != NULL),
        (SELECT Name, Amount FROM Opportunities WHERE stagename = 'Closed Won') FROM Account
        WHERE Id IN (SELECT AccountId FROM Opportunity where Amount != NULL AND Amount != 0 AND stagename = 'Closed Won')
     */
global class EmailopportunityRecord implements
    Database.Batchable<sObject>, Database.Stateful{
    global Map<Id,Decimal> listOfOpp=new Map<Id,Decimal>();
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(
            'SELECT ID,Name,AccountId,Account.Name,Amount,CloseDate'+
                'FROM Opportunity WHERE StageName =\'Closed Won\' AND CloseDate=today'+
                'AND AccountId != NULL AND Amount != NULL'
        );
    }
    global void execute(Database.BatchableContext bc, List<Opportunity> scope){
        AggregateResult[] aggrAmount= [SELECT
                                            AccountId,
                                            SUM(Amount)
                                        FROM
                                            Opportunity
                                        WHERE
                                            Id =: scope group by AccountId];
        for(AggregateResult aggr : aggrAmount){
                listOfOpp.put((Id)aggr.get('AccountId'),(Decimal)aggr.get('expr0'));
        }

    }

    global void finish(Database.BatchableContext bc){
        List<Messaging.SingleEmailMessage> EmailAlertAmt = new List<Messaging.SingleEmailMessage>();
        for(Id Idrec :listOfOpp.keySet())
        {
            for(Contact con : [SELECT
                                    Email,Name,Account.Name
                                FROM
                                    Contact
                                WHERE
                                    AccountId =: Idrec]){
                    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                    String[] toAddresses = new String[] {con.Email};
                    email.setToAddresses(toAddresses);
                    email.setSubject('Details of Opportunity Closed Today');
                    String htmlBody= '';

                    htmlBody = '<table width="100%" border="0" cellspacing="0" cellpadding="10"'+
                             'align="center" bgcolor="#F7F7F7">'+
                        +'<tr>'+
                        +'<td style="font-size: 14px; font-weight: normal;'+
                        'font-family:Calibri;line-height: 20px; color: #333;"><br />'+
                        +'<br />'+
                        +'Dear, '+con.Account.Name+',</td>'+
                        +'</tr>'+
                        +'<tr>'+
                        +'<td style="font-size: 14px; font-weight: normal;'+
                        'font-family:Calibri;line-height: 18px; color: #333;">Your Aggrigate result</td>'+
                        +'</tr>'+
                        +'</table>';

                    htmlBody +=  '<table border="1" style="border-collapse: collapse">'+
                                '<tr><th>Contact Person</th><th>Sum Of Total Opportunity</th></tr>';
                        Decimal AggrSUM = listOfOpp.get(Idrec);
                        String ContactName = con.Name;
                        htmlBody +='<tr><td>' + ContactName + '</td><td>' + AggrSUM +'</td><td>';
                    htmlBody += '</table><br>';
                    email.setHtmlBody(htmlBody);
                    EmailAlertAmt.add(email);
                }
            }
        Messaging.sendEmail(EmailAlertAmt);
    }



      /*  System.debug('Number of records fetched!' + count);
        AsyncApexJob job = [SELECT Id, Status, NumberOfErrors,
                            JobItemsProcessed,
                            TotalJobItems, CreatedBy.Email
                            FROM AsyncApexJob
                            WHERE Id = :bc.getJobId()];
       // System.debug(job);
      //  System.debug(scope);
        AggregateResult[] lstOppAvg = [
                                        SELECT
                                            AVG(Amount)
                                        FROM
                                            Opportunity
                                        WHERE
                                            StageName = 'Closed Won'
                                            AND
                                            CloseDate = today
                                    ];
        Object average = lstOppAvg[0].get('expr0');

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {job.CreatedBy.Email};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Closed Won Opportunities ' + job.Status);
            mail.setPlainTextBody('Number of Opportunities ' + count + '\n having total job items ' +
                job.TotalJobItems + '\n with '+ job.NumberOfErrors + ' failures.\n' +  lstOpp +
                '\n Average Amount = '+ average);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); */
}