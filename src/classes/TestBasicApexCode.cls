@isTest
public class TestBasicApexCode {

   // BasicApexCode bac = new BasicApexCode();
//Assignment 1 :Prime Number checking
    static testmethod void checkprime() {
        BasicApexCode bac = new BasicApexCode();
        Boolean val_positive = bac.prime(5);
        Boolean val_negative = bac.prime(6);
        System.assertEquals(val_positive,true);
        System.assertEquals(val_negative,false);
    }
//Assignment 2 : String is palindrome or Not
        static testmethod void checkpalindrome() {
        BasicApexCode bac = new BasicApexCode();
        Boolean val_positive = bac.palindrome('viv');
        Boolean val_negative = bac.palindrome('vivek');
        System.assertEquals(val_positive,false);
        System.assertEquals(val_negative,true);

    }
//Assignment 3: Method to replace every occurrence of 'a' character from each string in an array
        static testmethod void Testreplace() {
        BasicApexCode bac = new BasicApexCode();
        String val = bac.replace('v','r','vivek');
        System.assertEquals(val,'rirek');
    }
 //Assignment 4: Method to count the length of a string (not using length function)
        static testmethod void TestStringtength() {
        BasicApexCode bac = new BasicApexCode();
        Integer val = bac.lengthstr('vivek');
        System.assertEquals(val,5);
    }
//Assignment 5: Method to find only the unique numbers in an array
    static testmethod void TestUniqueArray() {
        BasicApexCode bac = new BasicApexCode();
        List <Integer> test_Array = new List <Integer>{1,2,3,3};
        Set <Integer> test_Result = new SET <Integer>{1,2,3};
        Set <Integer> val = bac.uniquenum(test_Array);
        System.assertEquals(val,test_Result);
    }

}