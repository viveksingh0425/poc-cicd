/****************************************************************************************
 * Class : Fibbonacis
 * Created By : Vivek Singh
 ----------------------------------------------------------------------------------------
 * Description : Display the Fibbo number upto user entered value
 ----------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        12/07/2018       Initial Apex Learning
 ***************************************************************************************/


public class fibbo {
    public static void fibbon(Integer n){
        Integer n1=0,n2=1,n3;    
 		System.debug(''+n1+''+n2);//printing 0 and 1
 		for(Integer i=2;i<n;++i)//loop starts from 2 because 0 and 1 are already printed    
        {
              n3=n1+n2;
              System.debug(''+n3);
              n1=n2;
              n2=n3;
        }
	}
}