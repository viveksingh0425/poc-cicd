public with sharing class Error_Logs {
	public static void Error_Logs_method() {

		Lead[] lead_record = new List<Lead>();
		logs__c[] log_obj = new List<logs__c>();
		Lead ab = new Lead(lastName='Testlogerror');
		for(Integer i=5;i<10;i++) {
			Lead a = new Lead(lastName='Test' + i,Company = 'Codetrunk', Status = 'Open-Not Contacted');
			lead_record.add(a);
		}
		lead_record.add(ab);
		Database.SaveResult[] db_result = database.insert(lead_record,false);
		for(Database.SaveResult dbr : db_result)
		{
			if(dbr.isSuccess()){
				System.debug('Success full record id :'+dbr.getId());
			}
			else
			{
				for(Database.Error err : dbr.getErrors())
				{
					logs__c log_record = new logs__c(Name='errr' ,Errors__c = 'err.getMessage()');
					log_obj.add(log_record);
					System.debug('Error Message : '+err.getMessage()+'Error Status Code'+err.getStatusCode()+'Error on field'
								+err.getFields());
				}
			}
		}
		System.debug(log_obj);
		Database.SaveResult[] log_ress = database.insert(log_obj,false);
	}
}


/*
Integer a = (System.TODAY() - BLANKVALUE(LastActivityDate, System.TODAY()));
System.debug(a);
Date d = today();
Date activitydateres = d.addDays(-90);
select id from Account where Active__c = 'Yes' AND 

SELECT Id FROM Account WHERE Active__c = 'NO'  AND CreatedDate = LAST_90_DAYS

Write a SOQL query on Contact to retrieve all active contacts belonging to 'media' Industry. Also
display the name of the account to which it is associated to.

*/