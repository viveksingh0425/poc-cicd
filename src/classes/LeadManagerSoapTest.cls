/**************************************************************************************************
 * Test Class : LeadManagerSoapTest
 * Created By : Vivek Singh
 --------------------------------------------------------------------------------------------------
 * Description : Lead Manipulator Service Using REST.
 * Class : LeadManagerSoap
 --------------------------------------------------------------------------------------------------
 * Version History:
 * Version  Developer Name    Date          Detail Features
 * 1.0      Vivek Singh     11/08/2018    Initial WebService Development
 **************************************************************************************************/

// Test class for the previous class.
@isTest
private class LeadManagerSoapTest {
  /**
    Test getLeadById Webservice
   */
  testMethod static void testgetLeadById() {
    String recordId = testData();
    System.debug(recordId);
    Lead ld = LeadManagerSoap.getLeadById(recordId);
    System.debug(ld);

    System.assert(ld != null);
    System.assertEquals(ld.Id,recordId);
  }
  /**
    Test createLead Webservice
   */
  testMethod static void testcreateLead() {
    Lead ld = LeadManagerSoap.createLead('Manoj Inc','Open - Not Contacted','Manoj','Cheenath',
                    '1258693','manoj@gmail.com');
    System.assert(ld != null);
    System.assertEquals(ld.Company,'Manoj Inc');
    System.assertEquals(ld.Status,'Open - Not Contacted');
    System.assertEquals(ld.FirstName,'Manoj');
  }
  /**
    Test deleteLead Webservice
   */
  testMethod static void testdeleteLead() {
    String recordId = testData();
    LeadManagerSoap.deleteLead(recordId);
    List<Lead> Leads = [SELECT Id FROM Lead WHERE Id=:recordId];
    System.assert(Leads.size() == 0);
  }
  /**
    Test upsertLead Webservice
   */
  testMethod static void testupsertLead() {
    String recordId = testData();
    Lead ld = LeadManagerSoap.upsertLead(recordId,'Manoj Inc upsert','Open - Contacted','Manoj test',
                'Cheenath','1258693','manoj@gmail.com');
    System.assert(ld != null);
    System.assertEquals(ld.Company,'Manoj Inc upsert');
    System.assertEquals(ld.Status,'Open - Contacted');
    System.assertEquals(ld.FirstName,'Manoj test');
  }
  /**
    Test updateLeadFields Webservice
   */
  testMethod static void testupdateLeadFields() {
    String recordId = testData();
    Lead ld = LeadManagerSoap.updateLeadFields(recordId,'Manoj test',
                'Cheenath','manojtestt@gmail.com');
    System.assert(ld != null);
    System.assertEquals(ld.LastName,'Cheenath');
    System.assertEquals(ld.Email,'manojtestt@gmail.com');
    System.assertEquals(ld.FirstName,'Manoj test');
  }
   /**
    Test Data Creation
   */
  Public Static Id testData(){
        Lead ldrec = new Lead();
        ldrec.Company = 'Test Company';
        ldrec.Status = 'Open - Not Contacted';
        ldrec.FirstName = 'First Test';
        ldrec.LastName = 'Last Test';
        ldrec.Phone = '123232342';
        ldrec.Email = 'vivek@gmail.com';
        insert ldrec;
        return ldrec.Id;
  }
}