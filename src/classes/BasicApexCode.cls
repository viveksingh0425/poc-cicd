// Day1 -Apex Assignment basics
public class BasicApexCode {

    //Assignment 1 :Prime Number checking
    public Boolean prime(integer num){
    for(integer i = 2; i < num; i++)
    {
        if(math.mod(num,i) == 0)
        {
            System.debug(num + ' is not prime');
            return false;
        }
    }
    System.debug(num + ' is prime');
    return true;
    }

    //Assignment 2 : String is palindrome or Not
    public Boolean palindrome(String strSample) {
		String strRever = strSample.reverse();
		if(strRever.equals(strSample))
		{
			System.debug('String is Palindrome');
			return false;
		}
		System.debug('String is not Palindrome');
   		return true;
    }

    //Assignment 3: Method to replace every occurrence of 'a' character from each string in an array
    public String replace(String str_replace_to,String str_replace_with,String str_word) {
		integer index = str_word.indexOf(str_replace_to);
		while (index >= 0)
		{
    		System.debug(index);
			str_word = str_word.replace(str_replace_to,str_replace_with);
    		index = str_word.indexOf(str_word, index + 1);
		}
		System.debug(str_word);
		return str_word;
    }

    //Assignment 4: Method to count the length of a string (not using length function)
    public Integer lengthstr(String str) {
			integer res=0;
			List<String> myArray = str.split(''); //Spliting the String
			for(Integer i=0;i<myArray.size();i++)
			{
				res++;
			}
			System.debug(res);
			return res;
	}

    //Assignment 5: Method to find only the unique numbers in an array
	public Set <Integer> uniquenum(List <Integer> list_Array) {

		Set<Integer> uniqueValues = new Set<Integer>(list_Array);
		System.debug(uniqueValues);
		return uniqueValues;
	}
    /* CALLING Method for Assignment 5
		List <String> list_Array = new List<String>{'a','b','c'};
        uniquenum.uniquenum(list_Array); */

}