/****************************************************************************************
 * Class : Account_With_Contact
 * Created By : Vivek Singh
 -------------------------------------------------------------------------------------------
 * Description : Create a Account Record with Name =”Eternus”. Create associated
				 contacts.
 -------------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        13/07/2018       Initial Apex Learning
 ***************************************************************************************/


public with sharing class Account_With_Contact {
	public static void Account_With_Contact_Val() {

		try
		{
			Account acct = new Account(
				Name='vivek111',
				Phone='(022)555-1212',
				BillingCity='Pune');
			insert acct;
			ID acctID = acct.ID;
			// Add a contact to this account.
			Contact con_market = new Contact(
				FirstName='sss',
				LastName='Rss',
				Phone='(022)555-1212',
				Department='Marketing',
				AccountId=acctID);
			Contact con_salesforce  = new Contact(
				FirstName='ss',
				LastName='ssssss',
				Phone='(022)555-1212',
				Department='Marketing',
				AccountId=acctID);
			List<Contact> conn = new List<Contact>();
			conn.add(con_salesforce);
			conn.add(con_market);
			//database.insert(conn,false);
			insert conn;
			System.debug(con_salesforce.id);
			System.debug(con_market.id);
			//System.debug(con_salesforce.Contact.id);
			//insert con_market,con_salesforce;
			// Add account with no contact
			Account acct2 = new Account(
				Name='Eternus Check',
				Phone='(022)555-1213',
				NumberOfEmployees=50,
				BillingCity='Mumbai',
				Description='Expert in wing technologies.');
		//	insert acct2;
		}catch(DMLException ex){
			System.debug('Error : '+ex);
		}

	}
}