/**************************************************************************************************
 * Class : EmailopportunityRecordScheduler
 * Created By : Vivek Singh
 --------------------------------------------------------------------------------------------------
 * Description : Scheduler Class for running job in each minute.
 * Test Class : EmailopportunityRecordTest
 * Class : EmailopportunityRecord
 --------------------------------------------------------------------------------------------------
 * Version History:
 * Version  Developer Name    Date          Detail Features
 * 1.0      Vivek Singh     08/06/2018    Initial Development
 **************************************************************************************************/

global with sharing class EmailopportunityRecordScheduler {

        global void execute(SchedulableContext sc){
        ID BatchId = Database.executeBatch(new EmailopportunityRecord(), 2);
        System.debug('Batch ID for Chunk'+BatchId);
    }
}

/* RUN Following Method in Anonymous Window of Salesforce
        EmailopportunityRecordScheduler batchSchedulerObj = new EmailopportunityRecordScheduler();
        /**
            CronJobInterval : '23rd hours job run'
         */
    /*
        String CronJobInterval = '0 0 23 * * ? *';
        String jobID = system.schedule('EmailopportunityRecord In Mid Night', CronJobInterval, batchSchedulerObj);
*/