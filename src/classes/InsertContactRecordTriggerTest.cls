/***************************************************************************************************
 * Class : TestClass for InsertContactTrigger
 * Created By : Vivek Singh
 ---------------------------------------------------------------------------------------------------
 * Description : This class test the functionality of 'InsertContactTrigger'.
 * ProblemStatement : ABC Containers require the ability to automatically associate a Contact created
                      in their Salesforce Instance with the respective Account based on the email
                      domain specified in the primary email address of the Contact. The association
                      should happen real time as soon as a Contact record is created within the
                      system.
 ---------------------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        23/07/2018       Initial Apex Trigger Learning
 **************************************************************************************************/

@isTest
private class InsertContactRecordTriggerTest
{
    static testMethod void TestContacttrigger()
    {
        //POC CICD Testing
        // Testing version 1.2
        testDataFactoryTrigger testdata = new testDataFactoryTrigger();
        //createAcRecPositive(Integer numAccts)
        testdata.createAcRecPositive(2); //Possitive test Account record
        //createAcRecNegative(Integer numAccts)
        testdata.createAcRecNegative(4); //Negative test Account record
        //createConRecPositive(Integer numAccts)
        testdata.createConRecPositive(2); //Possitive test Contact record
        //createConRecNegative(Integer numAccts)
        testdata.createConRecNegative(4); //Negative test Contact record
        List<Account> lstac = [SELECT
                                    id,Website_Domain__c
                                    FROM
                                    Account];
        List<String> lstEmail = new List<String>();
        for(Account ac: lstac){
            lstEmail.add(ac.Website_Domain__c);
        }
        System.debug(lstEmail);
        List<Contact> lstcon = [SELECT
                                    Id,AccountId,Email_domain__c
                                FROM
                                    Contact
                                WHERE
                                    Email_domain__c IN: lstEmail];
        System.debug(lstcon.size());
        System.assertEquals(4,lstcon.size());
    }
}