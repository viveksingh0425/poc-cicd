/**************************************************************************************************
 * ControllerClass : ContactSearchController
 * Created By : Vivek Singh
 --------------------------------------------------------------------------------------------------
 * Description : Implement “alphabetical search” on the related contacts page. The page should contain a
                 strip of all the alphabets on the top. On clicking the alphabet, the contact list should
                 be refreshed without reloading the actual page.
 * Test Class : ContactSearchControllerTest
 * VFPage : AlphabeticSearchPage
 --------------------------------------------------------------------------------------------------
 * Version History:
 * Version  Developer Name    Date          Detail Features
 * 1.0      Vivek Singh     29/07/2018    Initial Development
 **************************************************************************************************/

public with sharing class ContactSearchController{

    public List<String> SerchAlpabet{get;set;}
  public String SearchVar{get;set;}
  public list<contact> con{set;get;}
  public list<contact> ShowCon{set;get;}
  public integer sizee{get;set;}
    
   /**
     * Method will implement alphabetical search
     *
     * @Param NULL
     *
     * @return Nothing
     */
  public ContactSearchController() 
  { 
    SerchAlpabet=new List<string>{'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','All'};
    con=new List<Contact>();
    con=[SELECT name,email,MobilePhone,Birthdate from contact order by Name];
    SearchVar='All';
    ShowCon=new List<Contact>();
    for(Contact c:con)
    {
      if(SearchVar.equals('All'))
        ShowCon.add(c);
      else 
        if(c.Name.startsWith(SearchVar.toLowerCase())||c.Name.startsWith(SearchVar))
        ShowCon.add(c);
    }
    sizee=ShowCon.size(); 
    
  }
  /**
     * Method will Display the required result based on Alphabates Coming from VF Page as a Input
     *
     * @Param NULL
     *
     * @return Nothing
     */  
  public PageReference display() 
  {
    system.debug('___________________SearchVar_______________________________'+SearchVar);
    ShowCon.clear();
    for(Contact c:con)
    {
      if(SearchVar.equals('All'))
        ShowCon.add(c);
      else
        if(c.Name.startsWith(SearchVar.toLowerCase())||c.Name.startsWith(SearchVar))
        {
           ShowCon.add(c);
           system.debug('_______________if ______C__________________'+c);
        }   
       
    }
    sizee=ShowCon.size(); 
    return null;
  }
}