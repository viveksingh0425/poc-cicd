/****************************************************************************************
 * Class : Areaclass
 * Created By : Vivek Singh
 ----------------------------------------------------------------------------------------
 * Description : Abstract methode and overriding of Abstract methode
 ----------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        12/07/2018       Initial Apex Learning
 ***************************************************************************************/


public class Apexarea extends AreaofShape
{
  public Apexarea()
    {
    //Calling constructor of parent abstract class using super keyword
    	super(5);
    }
    // Calling non abstract method of parent
    public decimal Findcirclearea(integer r)
    {
    	return circleArea(r);
    }
    // This class must use override keyword for abstract methods.
    // Note area variable not defined in this class and still used
    //as it is defined in parent.
    public override decimal RectangleArea(decimal length, decimal width)
    {
        area = length * width ;
		return area;
    }
    public override decimal hexagonArea(decimal length)
    {
        area = 6*length;
        return area;
	}
	//this class can have its own methods also
	public decimal calculateSquareArea()
    {
        // I can't just call Constructor of Parent in child
        //in any method ,/ super needs to be present in constructor
        //of child
        // area = super(length)// this line will give error,if uncommented
        // area calculated by constructor, will be returned
        // from below statement.
        return area ;
	}
}


/* Calling
Apexarea obj = new Apexarea();
Double v = obj.hexagonArea(11.2);
System.debug(v);
Double v1 = obj.RectangleArea(10.0,10.0);
System.debug(v1);
Double v2 = obj.circleArea(10);
System.debug(v2);
 */