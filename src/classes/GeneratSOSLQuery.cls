public with sharing class GeneratSOSLQuery {
    Public String selectedObject {get; set;} // object selected
    Public List<SelectOption> objFields {get;set;}
    Public List<String> selectedFields {get;set;} //fields selected
    Public String limitSOSL {get;set;} // SOSL limit
    Public String order {get;set;} // order by value
    Public String searchString {get;set;} //text to find
    Public String SOSLQuery {get; set;} //SOSL query to run
    Public List<sObject> SOSLResult {get;set;}
    Public List<string> lstFlds {get;set;}
    Set<String> originalvalues = new Set<String>{'None'};
    Public List<string> leftselected{get;set;}
    Public List<string> rightselected{get;set;}
    Set<string> leftvalues = new Set<string>();
    Set<string> rightvalues = new Set<string>();
     
    
    
    public GeneratSOSLQuery (){
        leftselected = new List<String>();
        rightselected = new List<String>();
        leftvalues.addAll(originalValues);
    }
     
    public PageReference selectclick(){
        rightselected.clear();
        for(String s : leftselected){
            leftvalues.remove(s);
            rightvalues.add(s);
        }
        return null;
    }
     
    public PageReference unselectclick(){
        leftselected.clear();
        for(String s : rightselected){
            rightvalues.remove(s);
            leftvalues.add(s);
        }
        return null;
    }
 
    public List<SelectOption> getunSelectedValues(){
        List<SelectOption> options = new List<SelectOption>();
        List<string> tempList = new List<String>();
        tempList.addAll(leftvalues);
        tempList.sort();
        for(string s : tempList)
            options.add(new SelectOption(s,s));
        return options;
    }
 
    public List<SelectOption> getSelectedValues(){
        List<SelectOption> options1 = new List<SelectOption>();
        List<string> tempList = new List<String>();
        tempList.addAll(rightvalues);
        tempList.sort();
        for(String s : tempList)
            options1.add(new SelectOption(s,s));
        return options1;
    }

    
    public List<SelectOption> getObjectNames(){
        List<Schema.sObjectType> objectNames = Schema.getGlobalDescribe().Values();
        List<SelectOption> options = new List<SelectOption>();
        Map<String,String> sortedObj = new Map<String,String>();
        List<String> sortedObjList = new List<String>();
        options.add(new SelectOption ('--None--','--None--'));
        Integer num1, num2;
        num1=0;
        num2=0;
        for(Schema.sObjectType objName : objectNames){
            Schema.DescribeSObjectResult objResult = objName.getDescribe();
            num1++;
            if(objResult.isQueryable()){
                num2++;
                //system.debug(objResult.isQueryable());
                sortedObj.put(objName.getDescribe().getLabel(),objName.getDescribe().getName());
                
            }
        }
        sortedObjList.addAll(sortedObj.keyset());
        sortedObjList.sort();
        for(String sortKey : sortedObjList){
            options.add(new SelectOption(sortedObj.get(sortKey),sortKey));
            
        }
        
        return options;
    }
    public void getFields(){
        leftvalues.clear();
        originalvalues.clear();
        objFields = new List<SelectOption>();
        Map<String,Schema.sObjectType> allObjects = Schema.getGlobalDescribe();
        
        DescribeSObjectResult objDescribe = allObjects.get(selectedObject).getDescribe();
        if(objDescribe != null){
            Map<String,sObjectField> fieldMap = objDescribe.fields.getmap();
            List<String> sortedFields = new List<String>();
            for(String fname : fieldMap.keySet()){
                    sortedFields.add(fname);
            }
            sortedFields.sort();
           
            for(String sortedField : sortedFields){
                objFields.add(new SelectOption (fieldMap.get(sortedField).getDescribe().getName(),sortedField));
                originalvalues.add(fieldMap.get(sortedField).getDescribe().getName());
                System.debug(originalvalues);
                System.debug(sortedField);
            }
        }
        leftvalues.addAll(originalvalues);
    }
    
    public void runSOSL(){
    
        System.debug(searchString);
        System.debug(selectedObject);
        System.debug(rightselected);
        System.debug(getSelectedValues());
        System.debug(order);
        System.debug(limitSOSL);
        String fields = '';
        Integer j = 0 ;
        for (SelectOption s : getSelectedValues()) {
           if (j != 0) {
              fields = fields + ',';
           }
           fields = fields + s.getValue();
           ++j;
        }

        System.debug('Field - '+fields);
        try{
           
             
            SOSLQuery = 'FIND \''+ searchString +'\' IN ALL FIELDS '+
                                'RETURNING '+ selectedObject +'('+ fields +' ORDER BY createdDate '+ order +' LIMIT '+ limitSOSL +')';
                   
                System.debug(SOSLQuery);
                List<List<sObject>> results = search.query(SOSLQuery);
    
                System.debug('results -'+results);
                
                SOSLResult = results[0];
                System.debug(results);
                 Set<string> setFlds = new Set<String>();
                for(integer i=0;i<SOSLResult.size();i++){
                    setFlds.addAll(SOSLResult[i].getPopulatedFieldsAsMap().keySet());
                 }
                lstFlds = new List<String>();
                lstFlds.addAll(setFlds);
                system.debug(lstFlds);
        }catch(Exception e){
            System.debug(e);
        }


            
    }
    


}