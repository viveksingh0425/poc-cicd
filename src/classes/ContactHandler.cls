public with sharing class ContactHandler {

    public static void ContactHandlermethod(Contact[] conbject)
    {
        List<String> contactEmaildomains = new List<String>();
        for(Contact contact:conbject){
            contactEmaildomains.add(contact.Email_domain__c);
        }

        Map<String, Id> domains = new Map<String, Id>();
        for(Contact record: conbject) {
            domains.put(record.Email_domain__c, null);
        }
        for(Account record: [SELECT
                                Website_Domain__c
                            FROM Account
                            WHERE
                                Website_Domain__c IN :domains.keySet()]) {
            domains.put(record.Website_Domain__c, record.Id);
        }
        for(Contact record: conbject) {
            if(domains.get(record.Email_domain__c) != null) {
                record.AccountId = domains.get(record.Email_domain__c);
            } // you can figure out an else here, if you want
        }
    }


}