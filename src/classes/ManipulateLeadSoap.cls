global with sharing class ManipulateLeadSoap {

    // Wrapper for output
    global class ActionResponse {
        @TestVisible webservice Boolean isSuccess;
        @TestVisible webservice String lead;
        @TestVisible webservice String status;
    }

    //Webservice to insert Lead
    webservice static ActionResponse doInsert(
        String firstName, String lastName, String phone, String email, String company
    ) {
        Lead ld = new Lead();
        ActionResponse rsp = new ActionResponse();
        try {
            ld.FirstName = firstName;
            ld.LastName = lastName;
            ld.Phone = phone;
            ld.Email = email;
            ld.Company = company;
            insert ld;
            rsp.isSuccess = true;
            rsp.lead = ld.Id;
            rsp.status = 'Success';
        }catch(DMLException ex) {
            rsp.isSuccess = false;
            rsp.lead = '';
            rsp.status = 'Error: ' + ex.getMessage();
        }
        return rsp;
    }

    //Webservice to delete Lead
    webservice static ActionResponse doDelete(
        String firstName, String lastName, String phone, String email, String company
    ) {
        ActionResponse rsp = new ActionResponse();
        // try{
            List<Lead> lstLeads = [
                SELECT Id, Name, Phone, Email, Company, isDeleted
                FROM Lead
                WHERE
                    FirstName =: firstName AND
                    LastName =: lastName AND
                    Phone =: phone AND
                    Email =: email AND
                    Company =: company
                LIMIT 1
            ];
            System.debug('======' + lstLeads);
            if(lstLeads <> null && !lstLeads.isEmpty() ) {
                delete lstLeads;
                rsp.isSuccess = true;
                rsp.lead = lstLeads[0].Id;
                rsp.status = 'Success';
            }else {
                rsp.isSuccess = false;
                rsp.lead = '';
                rsp.status = 'No Matching record found';
            }

        // }catch(DMLException ex) {
        //     rsp.isSuccess = false;
        //     rsp.lead = '';
        //     rsp.status = 'Error: ' + ex.getMessage();
        // }
        // catch (Exception ex){
        //     rsp.isSuccess = false;
        //     rsp.lead = '';
        //     rsp.status = 'Error: ' + ex.getMessage();
        // }
        System.debug('===isSucess===' + rsp.isSuccess);
        return rsp;
    }

    //Webservice to update Lead
    webservice static ActionResponse doUpdate(
        String firstName, String lastName, String phone, String email, String company
    ) {
        ActionResponse rsp = new ActionResponse();
        try {
            List<Lead> lstLeads = [
                SELECT 
                    Id, Name, Phone, Email
                FROM 
                    Lead
                WHERE
                    FirstName =: firstName AND
                    LastName =: lastName AND
                    Company =: company
                LIMIT 1
            ];

            if(lstLeads <> null && !lstLeads.isEmpty() ) {
                lstLeads[0].Phone = phone;
                lstLeads[0].Email = email;
                // System.debug('========' + lstLeads);
                Update lstLeads;
                // System.debug('========' + lstLeads);
                rsp.isSuccess = true;
                rsp.lead = lstLeads[0].Name;
                rsp.status = 'Success';
            }else {
                rsp.isSuccess = false;
                rsp.lead = '';
                rsp.status = 'No Exact Match found';
            }
        }catch(DMLException ex) {
            rsp.isSuccess = false;
            rsp.lead = '';
            rsp.status = 'Error: ' + ex.getMessage();
        }
        // catch (Exception ex){
        //     rsp.isSuccess = false;
        //     rsp.lead = '';
        //     rsp.status = 'Error: ' + ex.getMessage();
        // }
        return rsp;
    }
}