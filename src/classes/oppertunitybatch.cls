/**************************************************************************************************
 * Class : oppertunitybatch
 * Created By : Vivek Singh
 --------------------------------------------------------------------------------------------------
 * Description : Batch to send A email for oppertunity amount and  aggregate of it.
 * Test Class : oppertunitybatchTest
 --------------------------------------------------------------------------------------------------
 * Version History:
 * Version  Developer Name    Date          Detail Features
 * 1.0      Twinkle Panjabi   10/04/2017    Initial Development
 **************************************************************************************************/

public with sharing class oppertunitybatch {
	public oppertunitybatch() {
		
	}
}