/**************************************************************************************************
* Class : WeatherPdfControllerTest
* Created By : Vivek Singh
--------------------------------------------------------------------------------------------------
* Description : Test class for WeatherPdfController class.
--------------------------------------------------------------------------------------------------
* Version History:
* Version  Developer Name    Date          Detail Features
* 1.0      Vivek Singh       04/01/2019    Initial Development
**************************************************************************************************/
@isTest
public class WeatherPdfControllerTest {
    
    static testMethod void weatherPdfTest() {
        Weather_History__c objWeatherHistory = new Weather_History__c();
        objWeatherHistory.City_Name__c = 'Mumbai';
        objWeatherHistory.Start_Date__c = datetime.newInstance(2019, 1, 14, 12, 30, 0);
        objWeatherHistory.End_Date__c = datetime.newInstance(2019, 1, 16, 13, 30, 0);
        insert objWeatherHistory;
        
        PageReference pageRef = Page.WeatherPdf;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('city', objWeatherHistory.City_Name__c);
        ApexPages.currentPage().getParameters().put('start', String.valueOf(objWeatherHistory.Start_Date__c));
        ApexPages.currentPage().getParameters().put('end', String.valueOf(objWeatherHistory.End_Date__c));
        
        Test.setMock(HttpCalloutMock.class, new HttpMockWeatherCallOut());
        WeatherPdfController objWeatherPdfController = new WeatherPdfController();
    }
}