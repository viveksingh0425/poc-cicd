/****************************************************************************************
 * Class : All_Active_Users
 * Created By : Vivek Singh
 --------------------------------------------------------------------------------------------
 * Description : Write a SOQL query to retrieve/print all active Users. Prepare a Map having
 				 User Id as key and User record as value
 --------------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        16/07/2018       Initial Apex Learning
 ***************************************************************************************/

public with sharing class All_Active_User {
	public Map<Id, User> All_Active_User_test() {

		//Selecting Active User Details
        Map<Id, User> active_user_map = new Map<Id, User>([SELECT
                                                           Id,Name
                                                           FROM User
                                                           WHERE
                                                           IsActive = true]);
		System.debug(active_user_map);
        return active_user_map;
	}
}