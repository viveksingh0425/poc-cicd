/**************************************************************************************************
 * Class :DeleteDuplicateLead
 * Created By : Vivek Singh
 --------------------------------------------------------------------------------------------------
 * Description : Remove Duplicate Lead Record Based Lead Fileds Like Email,Name etc.
 * Test Class : DeleteDuplicateLeadTest
 * Scheduler Class : DeleteDuplicateLeadScheduler
 --------------------------------------------------------------------------------------------------
 * Version History:
 * Version  Developer Name    Date          Detail Features
 * 1.0      Vivek Singh     02/08/2018     Initial Development
 **************************************************************************************************/

global class DeleteDuplicateLead implements Database.Batchable<SObject>{

    /**
        Custome Setting :  Custome Setting for Making it Configurable
     */
    String Companyname = LeadField__c.getAll().get('Company').fieldValue__c;
    String EmailAddr = LeadField__c.getAll().get('Email').fieldValue__c;
    String phoneno = LeadField__c.getAll().get('Phone').fieldValue__c;
    String NameRecord = LeadField__c.getAll().get('Name').fieldValue__c;
     /**
        EmailMap : EmailMap<Email,<Lead Record>>
     */
    global Map<String , Lead> maprec = new Map<String , Lead>();
    /**
        Query :  Dynamic Query for any field check
     */
    String searchResultStr = 'SELECT Id,'+NameRecord+','+EmailAddr+','+phoneno+' FROM Lead'+
                'WHERE '+NameRecord+' != NULL AND '+EmailAddr+' != NULL AND '+phoneno+' != NULL' ;
    /**
        Batch Start Method :  Send the query result to excute method
     */
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(searchResultStr);
    }

     /**
        Batch execute Method : logical Implementation for deleting dublicate value
     */
    global void execute(Database.BatchableContext BC , List<Lead> scope){
    System.debug(Scope);
    List<Lead> duplicatelist = new List<Lead>();
    /**
        Loop : Insert the Map Record and Make Sure that Email Should be unique
     */
    for(Lead ld : scope){
        String uniquestr = ld.Email+''+ld.Phone+''+ld.Name;
        if(! maprec.containsKey(uniquestr)){
            maprec.put(uniquestr , ld);
        }
        else{
            duplicatelist.add(ld);
        }
    }
        system.debug(duplicatelist);
		if(duplicatelist.size() > 0){
			delete duplicatelist;
		}
    }
    global void finish(Database.BatchableContext BC){
    }

}