public class DependentPicklistController {

public String states { get; set;}
public String country{ get; set;}
Map<String, city__c> c = city__c.getall();
public DependentPicklistController(){
    Map<String, city__c> c = city__c.getall();
    System.debug(c);
}

    public list<selectoption> getStateList() {
        list<selectoption> s = new list<selectoption> ();
        s.add(new selectoption('','Select one'));
        if(country==null){
            country='';
        }
        else if(c.containskey(country)){
            s.clear();
            string sm = c.get(country).country__c;
            for(string st: sm.split(';'))
            s.add(new selectoption(st,st));
        }
        return s;
    }

    public List<SelectOption> getCountrylist()
    {
        List<SelectOption> country = new List<SelectOption>();
        country.add(new selectoption('India','---SELECT ONE---'));
        list<string> countryname= new list<string>();
        countryname.addall(c.keyset());

        for(string s : countryname){
            country.add(new selectoption(s,s));
        }
        return country;
    }

}