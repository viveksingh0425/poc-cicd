/****************************************************************************************
 * Class : All_Contact_Records
 * Created By : Vivek Singh
 ----------------------------------------------------------------------------------------
 * Description : Query on all Contact records and add them to the List. Print that contents
 				 of this list.
 ----------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        16/07/2018       Initial Apex Learning
 ***************************************************************************************/

public with sharing class All_Contact_Records {
	public void All_Contact_Records_Test() {

		//Creating Map for Account and associated Contact Records for that Particular ID
		Map<Account,List<Contact>> accountContactMap = new Map<Account,List<Contact>>();
		List<Account> Account_record = [SELECT Id, name, (SELECT Id, Name FROM Contacts) FROM Account];
		for(Account acc : Account_record) //Iterating loop for all the Account Record
		{
			accountContactMap.put(acc, acc.Contacts);
		}
		System.debug(accountContactMap);
        
	}
}