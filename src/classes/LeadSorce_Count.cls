/****************************************************************************************
 * Class : LeadSorce_Count
 * Created By : Vivek Singh
 --------------------------------------------------------------------------------------------------------
 * Description : Find total number of distinct Lead records on basis of 'Lead Source' having greater than
 				 10 leads. Print this information.
 --------------------------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        16/07/2018       Initial Apex Learning
 ***************************************************************************************/

public with sharing class LeadSorce_Count {
	public LeadSorce_Count() {

	List<AggregateResult> lead_record =[SELECT
							LeadSource, count_distinct(Name)
							FROM Lead
							GROUP BY LeadSource HAVING count_distinct(Name) > 10];

	System.debug(lead_record);
	}
}