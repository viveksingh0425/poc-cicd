@isTest
public class TestDataFactory {
    public static void createTestAccountAndContactRecords(Integer numAccts, Integer numContactsPerAcct) {
        List<Account> accts = new List<Account>();
        for(Integer i=0;i<numAccts;i++) {
            Account a = new Account(Name='TestJohn' + i,Active__c = 'Yes',Industry = 'Media');
            accts.add(a);
        }
        insert accts;
        List<Contact> cons = new List<Contact>();
        for (Integer j=0;j<numAccts;j++) {
            Account acct = accts[j];
            // For each account just inserted, add contacts
            for (Integer k=numContactsPerAcct*j;k<numContactsPerAcct*(j+1);k++) {
                cons.add(new Contact(firstname='TestJohnrec'+k,
                                     lastname='Testjohn'+k,
                                     AccountId=acct.Id));
            }
        }
        // Insert all contacts for all accounts
        insert cons;
    }
    // opertunity Record creation based on opertunity Stage (parent Child Relationship)
    public static void createTestAccountAndOperRecords(Integer numAccts, Integer numOperPerAcct) {
        List<Account> accts = new List<Account>();
        for(Integer i=0;i<numAccts;i++) {
            Account a = new Account(Name='TestAoptee' + i);
            accts.add(a);
        }
        system.debug(accts.size());
        insert accts;
        List<Opportunity> lstOpertunity = new List<Opportunity>();
        for (Integer j=0;j<numAccts;j++) {
            Account acct = accts[j];
            // For each account just inserted, add Opertunity
            for (Integer k=numOperPerAcct*j;k<numOperPerAcct*(j+1);k++) {
                lstOpertunity.add(new Opportunity(Name='optAme' + k,CloseDate = Date.parse('7/23/2018'),
										 	StageName ='Closed Won',AccountId=acct.Id));
            }
        }
        System.debug(lstOpertunity);
        // Insert all opertunityrecor for all accounts
        insert lstOpertunity;
    }
    // Creating Active Users data
    public static void createTestUserRecords(Integer numofuserrec){
        Profile p = [select id from profile where name='Standard User'];
        List<User> UserRecord = new List<User>();
        for (Integer i=0;i<numofuserrec;i++) {
            User u = new User(alias = 'test123', email='test123@noemail.com',
                              emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',IsActive = true,
                              localesidkey='en_US', profileid = p.Id, country='United States',
                              timezonesidkey='America/Los_Angeles', username='test123@noemail.com'+i);
            UserRecord.add(u);
        }
        insert UserRecord;
    }
    //Create Lead Record for Nexted Map Code Test
    public static void createTestLeadRecords(){
         List<Lead> lstLead =   new List<Lead>{
                          new Lead(Company = 'Eternus', LastName = 'Mike', Status = 'Open'),
                          new Lead(Company = 'Nike', LastName = 'John', Status = 'Open',LeadSource = 'Web'),
                          new Lead(Company = 'Miles', LastName = 'Davis', Status = 'Open',LeadSource = 'Web'),
                          new Lead(Company = 'Reebok', LastName = 'Hillen', Status = 'Closed',LeadSource = 'Phone Inquiry'),
                          new Lead(Company = 'Addidas', LastName = 'Shrin', Status = 'Open',LeadSource = 'Phone Inquiry')
                         };  
        insert lstLead;
    }
    // Creating Number of contact records
    public static void createTestContactRecords(Integer numContactsPerAcct){
        List<Contact> cons = new List<Contact>();
        for (Integer i=0;i<numContactsPerAcct;i++) {
                cons.add(new Contact(firstname='Test'+i,
                                     lastname='Test'+i));
        }
         insert cons;
    }
    
    // Creating Number of opertunity Records having Amount Greater Than 10000
    public static void createTestOpertunityRecords(Integer numOperrecord){
        List<Opportunity> Opertunityrec = new List<Opportunity>();
        for(Integer i=0;i<numOperrecord;i++) {
            Opportunity a = new Opportunity(Name='TestAmount' + i,
                                            Amount=10200,
                                            CloseDate = Date.parse('7/23/2018'),
										 	StageName ='Closed Lost');
            Opertunityrec.add(a);
        }
         insert Opertunityrec;
    }

     // Creating Number of Account records
    public static void createTestAccountRecords(Integer numAcctK,Integer numAcctsM){
     
        List<Account> numAcctKe = new List<Account>();
        for(Integer i=0;i<numAcctK;i++) {
           // Account a = new Account(Name='TestAccountkerala'+i,
                                  //  BillingState = 'Kerala');
            Account a = new Account(Name='TestAccountkerala'+i);
            numAcctKe.add(a);
        }
        insert numAcctKe;
        List<Account> numAcctsMa = new List<Account>();
        for(Integer i=0;i<numAcctsM;i++) {
            Account a = new Account(Name='TestAccountMaharashtra' + i);
            numAcctsMa.add(a);
        }
        insert numAcctsMa;
    }
     // Account record for Enrollment_year 2010,2013,2014
    public static void createAccountEnrRecords(Integer en_yr2010,Integer en_yr2013,Integer en_yr2014){
     
        List<Account> enr_yr2010 = new List<Account>();
        for(Integer i=0;i<en_yr2010;i++) {
            Account a = new Account(Name='TestAccountkerala'+i,Enrollment_Year__c = '2010');
            enr_yr2010.add(a);
        }
        insert enr_yr2010;
        List<Account> enr_yr2013 = new List<Account>();
        for(Integer i=0;i<en_yr2013;i++) {
            Account a = new Account(Name='TestAccountMaharashtra' + i,Enrollment_Year__c = '2013');
            enr_yr2013.add(a);
        }
        insert enr_yr2013;
        List<Account> enr_yr2014 = new List<Account>();
        for(Integer i=0;i<en_yr2014;i++) {
            Account a = new Account(Name='TestAccountMaharashtra' + i,Enrollment_Year__c = '2014');
            enr_yr2014.add(a);
        }
        insert enr_yr2014;
    }
    // create Custome Records
    /*  public static void createCustomeTestRecords(Integer numCustomeRec){
     
        List<c__c> cRecord = new List<c__c>();
        for(Integer i=0;i<numCustomeRec;i++) {
            c__c c = new c__c(Name='John'+i,C_B_Relation__c = 'John'+i,C_A_Relation__c = 'John'+i);
            cRecord.add(c);
        }
        insert cRecord;
    } */
    
}