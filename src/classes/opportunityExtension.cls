/***************************************************************************************************
 * Class : opportunityExtension
 * Created By : Vivek Singh
 ---------------------------------------------------------------------------------------------------
 * Description :ABC Containers has recently moved to Salesforce and are interested to adopt
                the Invoicing process on Salesforce. The finance team would require an ability
                to generate invoices along with all its line items in a portable document format
                which they would be able to store and leverage for offline use.
                Implement a solution using the Force.com platform to cater to this requirement.
* PageName: GenereteInvoicePDF
 ---------------------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        19/07/2018       Initial Apex Trigger Learning
 **************************************************************************************************/

Public Class opportunityExtension{

    public static List<Opportunity> lstOppo{get; set;}
    public static List<OpportunityLineItem> lstOppoLine{get; set;}
    public static List<Quote> lstQuotes{get; set;}

    public static Id Ooppo_AccountId{get; set;}
    public static List<Contact> lstContacts{get; set;}

    public id oppoid{ get; set;}
    public String invoiceId{ get; set;}
    public String Str{ get; set;}
    //public List<Attachment> attList{ get; set;}
    /**
     * Method will implement Standard Controller opportunity
     *
     * @Param NULL
     *
     * @return Nothing
     */
    public opportunityExtension(ApexPages.StandardController controller){
        // get the current page Id
        oppoid = ApexPages.currentPage().getParameters().get('Id');
        System.debug(oppoid);
        /**
            Query which Retrive all the related Data based on Opportunity Record
         */
        lstOppo = [SELECT
                        Id,invoiceAmmount__c,
                        Closedate, Name,
                        Account.Name, Account.BillingCity,
                        Account.BillingStreet,Account.BillingPostalCode,
                        Account.BillingState,Account.phone,
                        (SELECT
                            Quantity, UnitPrice, TotalPrice,Description,
                            PricebookEntry.Name, PricebookEntry.Product2.Family
                        FROM
                            OpportunityLineItems
                        WHERE
                            OpportunityId =: oppoid),
                        (SELECT
                            Invoice_number__c,ExpirationDate,
                            Discount,TotalPrice,Tax,SubTotal,
                            CreatedDate
                        FROM
                            Quotes
                        WHERE
                            OpportunityId =: oppoid
                        ORDER by
                            CreatedDate DESC)
                    FROM
                        Opportunity
                    WHERE
                        Id =: oppoid];
        System.debug(lstOppo);
        /**
            Loop Iterate Over Opportunity and Store OpportunityLineItems & Quotes Record in List
         */
        for(Opportunity opp :lstOppo){
            lstOppoLine= opp.OpportunityLineItems;
            lstQuotes= opp.Quotes;
            Ooppo_AccountId= opp.Accountid;
        }
        System.debug('@@lstAccounts@@'+Ooppo_AccountId);
        /**
            Query which Retrive all the related Data based on Contact Record
         */
        lstContacts = [SELECT
                            Name, MailingStreet,
                            MailingCity, MailingState,
                            MailingPostalCode, MailingCountry,
                            MobilePhone, Email
                        FROM
                            Contact
                        WHERE
                            AccountId =: Ooppo_AccountId Limit 1];
        System.debug('@@oppLineItemList@@'+lstOppoLine);
        System.debug('@@lstContacts@@'+lstContacts);
    }
}