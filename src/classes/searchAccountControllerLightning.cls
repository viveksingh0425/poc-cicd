public class searchAccountControllerLightning {
    
    @AuraEnabled
 	  public static List < account > fetchAccount(String searchKeyword) {
      String searchKey = searchKeyword + '%';
      List < Account > returnList = new List < Account > ();
      List < Account > lstOfAccount = [select id, Name, Type, Industry, Phone, Fax from account
                                       where Name LIKE: searchKey LIMIT 500];
     
      for (Account acc: lstOfAccount) {
       returnList.add(acc);
      }
      return returnList;
 }

}