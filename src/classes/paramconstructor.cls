/****************************************************************************************
 * Class : Parameterized Constructor
 * Created By : Vivek Singh
 ----------------------------------------------------------------------------------------
 * Description : Parameterized Constructor Example
 ----------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        12/07/2018       Initial Apex Learning
 ***************************************************************************************/

public with sharing class paramconstructor {
	Integer RollNo;
	String Name;
	public paramconstructor(Integer rno, String st_name) {
		RollNo = rno;
		Name = st_name;
	}
	public void show_details()
	{
        String Student_details = 'Name:'+Name+'\tRoll Number : '+RollNo;
        System.debug(Student_details);
		System.debug('Student is'+Name);
		System.debug('Student rollno is'+RollNo);
       // return Student_details;
	} 

}


/*Calling

paramconstructor pr1 = new paramconstructor();
pr1.show();
paramconstructor pr2 = new paramconstructor(100,'rahul');
pr2.show();

*/