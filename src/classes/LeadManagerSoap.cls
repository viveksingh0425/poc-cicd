global class LeadManagerSoap {

    /**
        Get Lead Record WebService
     */
    webservice static Lead getLeadById(String recId) {
       // Lead ld = new Lead();
        Lead lstLead = new Lead();
        try{
           // ld.id = recId;
            lstLead = [SELECT
                            Company,FirstName,
                            LastName,Phone,Email
                        FROM
                            Lead
                        WHERE
                            Id =: recId
                        ];
        }catch(Exception e){
            System.debug('Error Message'+e.getMessage());
        }
        return lstLead;
    }

    /**
        Create Lead WebService
     */
    webservice static Lead createLead(String company, String status,
            String firstname,String lastname,String phone,String email) {
        Lead ld = new Lead();
        try{
            ld.Company = company;
            ld.Status = status;
            ld.FirstName = firstName;
            ld.LastName = lastname;
            ld.Phone = phone;
            ld.Email = email;
            insert ld;
        }catch(Exception e){
            System.debug('Error Message'+e.getMessage());
        }
        return ld;
    }

    /**
        Delete Lead Record WebService
     */
    webservice static void deleteLead(String recId) {
       // Lead ld = new Lead();
        List<Lead> lstLead = new List<Lead>();
        try{
           // ld.id = recId;
            lstLead = [SELECT
                            Company,FirstName,
                            LastName,Phone,Email
                        FROM
                            Lead
                        WHERE
                            Id =: recId
                        ];
            delete lstLead;
        }catch(Exception e){
            System.debug('Error Message'+e.getMessage());
        }
    }
    /**
        Upsert Lead WebService
     */
    webservice static Lead upsertLead(String recid,String company, String status,
            String firstname,String lastname,String phone,String email) {
        Lead ld = new Lead();
        try{
            ld.Id = recid;
            ld.Company = company;
            ld.Status = status;
            ld.FirstName = firstName;
            ld.LastName = lastname;
            ld.Phone = phone;
            ld.Email = email;
            upsert ld;
        }catch(Exception e){
            System.debug('Error Message'+e.getMessage());
        }
        return ld;
    }

    /**
        Update Lead WebService
     */
    webservice static Lead updateLeadFields(String recid,String firstname,String lastname,String email){
        Lead thisLead = new Lead();
        try{
            thisLead = [SELECT
                            Id
                        FROM
                            Lead
                        WHERE
                            id =: recid];
            thisLead.FirstName = firstName;
            thisLead.LastName = lastname;
            thisLead.Email = email;
            update thisLead;
        }catch(Exception e){
            System.debug('Error Message'+e.getMessage());
        }
        return thisLead;
    }
}


/* global class LeadManagerSoap {

    global class LeadInfo {
        webservice String company;
        webservice String status;
        webservice String firstName;
        webservice String lastname;
        webservice String phone;
        webservice String email;
    }

    webservice static Lead createLead(LeadInfo info) {
        Lead ld = new Lead();
        try{
            //Lead ld = new Lead();
            ld.Company = info.company;
            ld.Status = info.status;
            ld.FirstName = info.firstName;
            ld.LastName = info.lastname;
            ld.Phone = info.phone;
            ld.Email = info.email;
            insert ld;
        }catch(Exception e){
            System.debug('Error Message'+e.getMessage());
        }
        return ld;
    }
} */