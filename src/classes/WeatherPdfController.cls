/**************************************************************************************************
* Class : WeatherPdfController
* Created By : Vivek Singh
--------------------------------------------------------------------------------------------------
* Description : Display Weather Historical Data as PDF
* Test Class : WeatherPdfControllerTest
--------------------------------------------------------------------------------------------------
* Version History:
* Version  Developer Name    Date          Detail Features
* 1.0      Vivek Singh       04/01/2019    Initial Development
**************************************************************************************************/

public class WeatherPdfController {
    
    public List<WeatherWrapper> lstWeatherWrapper	{get;set;}
    
    public WeatherPdfController() {
        lstWeatherWrapper = new List<WeatherWrapper>();
        
        String city = ApexPages.currentPage().getParameters().get('city');
        DateTime startDate = DateTime.valueOf(ApexPages.currentPage().getParameters().get('start'));
        DateTime endDate = DateTime.valueOf(ApexPages.currentPage().getParameters().get('end'));
        
        List<Weather_History__c> lstWeatherHistory = new List<Weather_History__c>();
        lstWeatherHistory = [ SELECT id
                             , Start_Date__c
                             , End_Date__c
                             , City_Name__c
                             , Temperature__c
                             , Pressure__c
                             , Humidity__c
                             , Minimum_Temperature__c
                             , Maximum_Temperature__c
                             FROM Weather_History__c
                             WHERE City_Name__c = :city
                             AND (Start_Date__c >= :startDate
                                  AND Start_Date__c <= :endDate)
                            ];
        
        if(lstWeatherHistory.size() > 0) {
            for(Weather_History__c objWeather : lstWeatherHistory) {
                WeatherWrapper objWeatherWrapper = new WeatherWrapper(String.valueOf(objWeather.Start_Date__c)
                                                                      ,objWeather.Temperature__c
                                                                      ,objWeather.Pressure__c
                                                                      ,objWeather.Humidity__c
                                                                      ,objWeather.Minimum_Temperature__c
                                                                      ,objWeather.Maximum_Temperature__c);
                lstWeatherWrapper.add(objWeatherWrapper);
            }
        }
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('http://api.openweathermap.org/data/2.5/weather?q='
							+city+'&units=metric&appid=22cb465d835d3d9e292b39f22b4685af');
        request.setMethod('GET');
        
        HttpResponse response = http.send(request);
        
        if (response.getStatusCode() == 200 ) { 
            Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(response.getBody()); 
            for(String str : results.keyset()) {
                if(str == 'main') {
                    Map<String, Object> mainResult = (Map<String, Object>)(results.get(str));
                    
                    String weatherdate = String.valueOf(startDate);
                    String temp = String.valueOf(mainResult.get('temp'));
                    String pressure = String.valueOf(mainResult.get('pressure'));
                    String humidity = String.valueOf(mainResult.get('humidity'));
                    String temp_min = String.valueOf(mainResult.get('temp_min'));
                    String temp_max = String.valueOf(mainResult.get('temp_max'));
                    
                    WeatherWrapper objWeatherWrapper = new WeatherWrapper(weatherdate,temp,pressure,humidity,temp_min,temp_max);
                    lstWeatherWrapper.add(objWeatherWrapper);
                }
            }
        }
    }
    
    //Wrapper class to deserialize json data
    public class WeatherWrapper {
        public String weatherdate 	{get;set;}
        public String temp 			{get;set;}
        public String pressure 		{get;set;}
        public String humidity  	{get;set;}
        public String temp_min 		{get;set;}
        public String temp_max 		{get;set;}
        
        public WeatherWrapper( String weatherdate, String temp, String pressure, String humidity, String temp_min, String temp_max) {
            this.weatherdate = weatherdate;
            this.temp = temp;
            this.pressure = pressure;
            this.humidity = humidity;
            this.temp_min = temp_min;
            this.temp_max = temp_max;
        } 
    }
}