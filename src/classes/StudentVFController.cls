public class StudentVFController{

    public Student__c stud{get; set;}
    public string selectedLang{get;set;}
    public List<selectoption> listOfLang {get;set;}
    public StudentVFController(ApexPages.StandardController controller) {
        stud=new Student__c();
        selectedLang='en';

        listOfLang = new List<selectOption>();
        listOfLang.add(new selectOption('en','English'));
        listOfLang.add(new selectOption('es','Spanish'));
        listOfLang.add(new selectOption('fr','French'));
    }
    public pagereference Save(){
      /*  Student__c studrecord = new Student__c();
        studrecord.Name=stud.Name;
        studrecord.City__c=stud.City__c;
        studrecord.Roll_no__c=stud.Roll_no__c;
        studrecord.Gender__c=stud.Gender__c;
        studrecord.Course__c=stud.Course__c;
        studrecord.State__c=stud.State__c;
        studrecord.Country__c=stud.Country__c;
        studrecord.College__c=stud.College__c;
        studrecord.H_S_C__c=stud.H_S_C__c;
        studrecord.S_S_C__c=stud.S_S_C__c; */
        insert stud;
        //Pagereference pg = new Pagereference('/' + studrecord.id);
       // pg.setredirect(true);
        PageReference p = apexPages.currentPage();
        ApexPages.Message msg=new ApexPages.Message(ApexPages.Severity.CONFIRM,'Record Created Successfully.Thank you!');
        ApexPages.addMessage(msg);
        return p;
       // ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Record Created Successfully.Thank you!'));
       // return null;
    }
}