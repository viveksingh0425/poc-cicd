/**************************************************************************************************
 * Test Class : AccountSearchControllerTest
 * Created By : Vivek Singh
 --------------------------------------------------------------------------------------------------
 * Description : Test Class for AccountSearchPage & AccountSearchController
 * ControllerClass : AccountSearchController
 * VFPage : AccountSearchPage
 --------------------------------------------------------------------------------------------------
 * Version History:
 * Version  Developer Name    Date          Detail Features
 * 1.0      Vivek Singh     29/07/2018    Initial Development
 **************************************************************************************************/

@isTest
public class AccountSearchControllerTest{
    /**
     * Method will create & get Search Account Info for Possitive Testing
     *
     * @Param NULL
     *
     * @return Nothing
     */
    static testMethod void searchAccountPositiveTest() {
        test.startTest();
        Account acc = new Account();
        acc.Name = 'vivek';
        insert acc;
        
        Account act = new Account();
        Apexpages.StandardController sc = new Apexpages.standardController(act);
        
        AccountSearchController MyControllerObj = new AccountSearchController(sc);
        MyControllerObj.accSearch ='vivek';
        MyControllerObj.doSearch();
        MyControllerObj.clear();
        test.stopTest();
    }
     /**
     * Method will create & get Search Account Info for Negative Testing
     *
     * @Param NULL
     *
     * @return Nothing
     */
     static testMethod void searchAccountNegativeTest() {
        test.startTest();
        Account acc = new Account();
        acc.Name = 'vivek';
        insert acc;
        
        Account act = new Account();
        Apexpages.StandardController sc = new Apexpages.standardController(act);
        
        AccountSearchController MyControllerObj = new AccountSearchController(sc);
        MyControllerObj.accSearch ='naina';
        MyControllerObj.doSearch();
        MyControllerObj.clear();
        test.stopTest();
    }
    
    /**
     * Method will create & get Search Account Info for NULL Testing
     *
     * @Param NULL
     *
     * @return Nothing
     */
     static testMethod void searchAccountNullTest() {
        test.startTest();
        Account acc = new Account();
        acc.Name = 'vivek';
        insert acc;
        
         Account act = new Account();
        Apexpages.StandardController sc = new Apexpages.standardController(act);
        
        AccountSearchController MyControllerObj = new AccountSearchController(sc);
        MyControllerObj.accSearch ='';
        MyControllerObj.doSearch();
        MyControllerObj.clear();
        test.stopTest();
    }
}