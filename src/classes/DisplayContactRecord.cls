public with sharing class DisplayContactRecord {
    public List<Contact> conList {get;set;}
    public List<Opportunity> optList {get;set;}
	public DisplayContactRecord(ApexPages.StandardSetController controller) {
	}
    public PageReference ContactLists()
    {
    if(ApexPages.currentPage().getParameters().get('id') != null)
      conList = [SELECT
                    id,Name,Phone,Email
                FROM
                    contact
                WHERE
                    accountId =: ApexPages.currentPage().getParameters().get('id')];
       optList = [SELECT
                    id,Name,StageName,CloseDate
                FROM
                    Opportunity
                WHERE
                    accountId =: ApexPages.currentPage().getParameters().get('id')];
     return null;
    }
}