/****************************************************************************************
 * Class : Demonstration_of_MAP
 * Created By : Vivek Singh
 -------------------------------------------------------------------------------------------
 * Description : Write a code to demonstrate the use of map and its methods(Code should
 				 cover put, get, containskey, remove, keyset and values methods).
 -------------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        13/07/2018       Initial Apex Learning
 ***************************************************************************************/


public with sharing class Demostration_of_MAP {
	public Demostration_of_MAP() {

		Map<Integer,String> test = new Map<Integer,String>(); //Creating MAP
		test.put(1,'vivek');
		test.put(2,'rahul');
		String a = test.get(1);
		System.debug(test.Keyset());//Display all the keyset for MAP
		if(test.containskey(3)){ //If the third key is present in MAP then only if excute.
			System.debug(a);
			String res = test.remove(1); //Delete data from the MAP who have Key=1
		}
		System.debug(test.values()); // Display all the Map values
		System.debug(test);

	}
}