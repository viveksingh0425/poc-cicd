/****************************************************************************************
 * Class : Billing_State_ORDERBY_LIMIT_NULLS_LAST
 * Created By : Vivek Singh
 --------------------------------------------------------------------------------------------------------
 * Description :Write a SOQL query to find all Account records where 'Billing State' is not 'Maharashtra'
 				and 'Kerala'. Order the results by Billing State in descending order with null values at
				 the end. Display first 10,000 records only. NOTE: do not use AND operator.
 --------------------------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        16/07/2018       Initial Apex Learning
 ***************************************************************************************/

public with sharing class Billing_State_ORDERBY_LIMIT_NULLs_LAST {
	public Billing_State_ORDERBY_LIMIT_NULLs_LAST() {

		List<Account> ac_record = [SELECT
						ID,Name
						FROM Account
						WHERE
						BillingState NOT IN ('Kerala','Maharashtra')
						ORDER BY BillingState DESC NULLS LAST
						LIMIT 10000];
		System.debug(ac_record);
	}
}