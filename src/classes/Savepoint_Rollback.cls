/****************************************************************************************
 * Class : Savepoint_Rollback
 * Created By : Vivek Singh
 -------------------------------------------------------------------------------------------
 * Description : Code to demonstrate the use of Savepoint and Rollback.
 -------------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        13/07/2018       Initial Apex Learning
 ***************************************************************************************/

public with sharing class Savepoint_Rollback {
	public Savepoint_Rollback() {

		try
		{
			Account a = new Account(Name = 'xxx');
			insert a;
			// Create a savepoint while AccountNumber is null
			Savepoint sp = Database.setSavepoint();
			// Change the account number
			a.AccountNumber = '123';
			update a;
			System.assertEquals('123', [SELECT AccountNumber FROM
			Account WHERE Id = :a.Id].AccountNumber);
			// Rollback to the previous null value
			Database.rollback(sp);
			System.assertEquals(null, [SELECT AccountNumber FROM
			Account WHERE Id = :a.Id].AccountNumber);
		}catch(Exception e){
			System.debug('Error'+e);
		}
	}
}