public with sharing class uniquenum {
	public static Set <Integer> uniquenum(List <Integer> list_Array) {

		Set<Integer> uniqueValues = new Set<Integer>(list_Array);
		System.debug(uniqueValues);
		return uniqueValues;
	}
}

/* CALLING
		List <String> list1 = new List<String>{'a','b','c'};
        uniquenum.uniquenum(list1);
*/