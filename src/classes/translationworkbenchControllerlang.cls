public with sharing class translationworkbenchControllerlang {
    public string selectedLang{get;set;}
    public List<selectoption> listOfLang {get;set;}
    public translationworkbenchControllerlang(ApexPages.StandardController controller) {
        selectedLang='en';

        listOfLang = new List<selectOption>();
        listOfLang.add(new selectOption('en','English'));
        listOfLang.add(new selectOption('es','Spanish'));
        listOfLang.add(new selectOption('fr','French'));
    }
}