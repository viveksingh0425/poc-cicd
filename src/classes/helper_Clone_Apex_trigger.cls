/****************************************************************************************
 * Class : helper_Clone_Apex_trigger
 * Created By : Vivek Singh
 -------------------------------------------------------------------------------------------
 * Description : Helper Class for Write a Trigger on Lead which will create the clone record.
 -------------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        16/07/2018       Initial Apex Trigger Learning
 ***************************************************************************************/

public Class helper_Clone_Apex_trigger
{
    private static boolean Val = true;
    public static boolean runOnce()
    {
        if(Val)
        {
            Val=false;
            return true;
        }
        else
        {
            return Val;
        }
    }
}