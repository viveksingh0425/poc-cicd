@isTest
public class OpportunityBatchTest {
    static testMethod void batchTest(){
        ID aId;
        List<Opportunity> lstOpp = new List<Opportunity>();
        
        List<Account> lstAccount = new List<Account>();
        Account acc = new Account();
        acc.Name = 'Rshakir';
        insert acc;
        
        List<Account> accId = [SELECT id from Account where Name='Rshakir'];
        for(Account ac : accId)
        {
            aId = ac.id;
        }
       
        Opportunity opp = new Opportunity();
        opp.Name ='New Opportunity';
        opp.StageName = 'Closed Won';
        opp.CloseDate = date.today();
        opp.AccountId = aId;
        lstOpp.add(opp);
        insert lstOpp;
        
        List<Contact> lstCon = new List<Contact>();
        Contact cnt = new Contact();
        cnt.LastName = 'New Contact';
        cnt.AccountId = aId;
        lstCon.add(cnt);
        insert lstCon;
        
        List<Opportunity> opl = [SELECT id, AccountId From Opportunity where Name = 'New Opportunity'];
        List<Contact> cnl = [SELECT id,AccountId From Contact where LastName = 'New Contact'];
        
        Integer count=0;
        for(Opportunity op : opl)
        {
            for(Contact cn : cnl)
            {
               if(op.AccountId == cn.AccountId)
                {
                    count+=1;
                } 
            }
        }
        
        Test.startTest();
           // ClosedWonOppBatch cwob = new ClosedWonOppBatch();
           // DataBase.executeBatch(cwob);
        Test.stopTest();
    }
}