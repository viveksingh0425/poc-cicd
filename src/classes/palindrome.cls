/****************************************************************************************
 * Class : Palindrome
 * Created By : Vivek Singh
 ----------------------------------------------------------------------------------------
 * Description : Check whether the String is palindrome or not
 ----------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        12/07/2018       Initial Apex Learning
 ***************************************************************************************/


public with sharing class palindrome {
	public static Boolean palindrome(String strSample) {
		String strRever = strSample.reverse();
		if(strRever.equals(strSample))
		{
			System.debug('String is Palindrome');
			return false;
		}
		System.debug('String is not Palindrome');
   		return true; 

	}
}