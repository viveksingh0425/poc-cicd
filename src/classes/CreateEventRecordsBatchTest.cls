@isTest
private class CreateEventRecordsBatchTest
{
    @testSetup 
    static void setup() {
        List<JSON_EVENT__c> jsonEventList = new List<JSON_EVENT__c>();

        for (Integer i = 0; i < 100; i++) {
           jsonEventList.add(
               new JSON_EVENT__c(
                   JSON__c = '{"JSON":{"Name":"A Test JSON 1"},"Object Name":"Account"}',
                   Status__c = 'New'
               )
           );
        }

        for (Integer i = 0; i < 40; i++) {
           jsonEventList.add(
               new JSON_EVENT__c(
                   JSON__c = '{"JSON":{"LastName":"A Test JSON 2"},"Object Name":"Contact"}',
                   Status__c = 'New'
               )
           );
        }

        for (Integer i = 0; i < 50; i++) {
           jsonEventList.add(
               new JSON_EVENT__c(
                   JSON__c = '{"JSON":{"LastName":"A Test JSON 2"},"Object Name":"Contactss"}',
                   Status__c = 'New'
               )
           );
        } 

        for (Integer i = 0; i < 60; i++) {
           jsonEventList.add(
               new JSON_EVENT__c(
                   JSON__c = '{"JSON":{"LastName":"A Test JSON 2", "Education__c":"ME"},'
                             + '"Object Name":"Contact"}',
                   Status__c = 'New'
               )
           );
        } 
		System.debug(jsonEventList);
        insert jsonEventList;
    }

  @isTest
  static void testBatch()
  {
        system.debug('======size======'+[select id from JSON_EVENT__c].size());
        Test.startTest();
            CreateEventRecordsBatchJSON b = new CreateEventRecordsBatchJSON(); 
            Id jobIds = Database.executeBatch(b,400);
      		System.debug(jobIds);
        Test.stopTest();

        // Check all inserted records
        System.assertEquals(140,[SELECT count() FROM JSON_EVENT__c WHERE Status__c = 'Complete']);

        // Check all error records
        System.assertEquals(110,[SELECT count() FROM JSON_EVENT__c WHERE Status__c = 'Error']);

        // Check all inserted Accounts
        System.assertEquals(100,[SELECT count() FROM Account]);

        // Check all inserted Contacts
        System.assertEquals(40,[SELECT count() FROM Contact]);
  }
}