/**************************************************************************************************
 * Class : LeadManager
 * Created By : Vivek Singh
 --------------------------------------------------------------------------------------------------
 * Description : Lead Manipulator Service Using REST.
 * Test Class : LeadManagerTest
 --------------------------------------------------------------------------------------------------
 * Version History:
 * Version  Developer Name    Date          Detail Features
 * 1.0      Vivek Singh     08/07/2018    Initial WebService Development
 **************************************************************************************************/

@RestResource(urlMapping='/Lead/*')
global with sharing class LeadManager {

    //Response Wrapper
    global class parse {
        public String StatusRec;
        public Boolean isSuccess;
        public String recId;
        public String recName;
        Lead ldrecord =new Lead();
        public String Error;
    }

    //Wrapper call parse resresult = new parse('hello','Success', true,result);


    //Reads or retrieves records.
    // Callout : /services/apexrest/Lead/<RecordId>
    @HttpGet
    global static parse getLeadById() {

        parse resparse = new parse();
        HttpResponse response = new HttpResponse();
        try{
            RestRequest request = RestContext.request;
            // grab the LeadId from the end of the URL
            String leadId = request.requestURI.substring(
            request.requestURI.lastIndexOf('/')+1);
            Lead result =  [SELECT
                                Status,Email,Company,FirstName,LastName
                            FROM
                                Lead
                            WHERE
                                Id = :leadId];
            resparse.StatusRec = 'Success';
            resparse.isSuccess = True;
            resparse.recId = leadId;
            resparse.ldrecord = result;
        }catch(Exception e){
            resparse.StatusRec = 'failure';
            resparse.isSuccess = False;
            response.getStatusCode();
            resparse.Error = 'Error'+e.getMessage();
        }
        return resparse;
    }


    //Creates records.
    /* Callout : /services/apexrest/Lead/
        JSON : {
                    "company" : "Codetrunk12",
                    "status" : "Open-Not Contacted",
                    "firstname" : "Vivek"
                    "lastname" : "guestadmin22"
                   // "RecId" : "RecordTypeId" // if there is no default RecordType is Set
               }
        createLead(String company, String status,String lastname,Id RecId)
    */
    @HttpPost
    global static parse createLead(String company, String status,
        String firstname,String lastname,String phone,String email) {
        Lead thisLead;
        parse resparse = new parse();
        HttpResponse response = new HttpResponse();
        try{
                thisLead = new Lead(
                    Company=company,
                    Status=status,
                    FirstName=firstname,
                    LastName=lastname,
                    Phone=phone,
                    Email=email
                );
                insert thisLead;
                resparse.StatusRec = 'Success';
                resparse.isSuccess = True;
                resparse.recId = thisLead.Id;
            /* RestContext.response.addHeader('Content-Type', 'application/json');
                RestContext.response.responseBody =
                Blob.valueOf('{ "Id" : "'+thisLead.Id+'", "isSuccess" : "True", "Status" : "Success" }'); */
            }catch(Exception e){
               /* RestContext.response.addHeader('Content-Type', 'application/json');
                RestContext.response.responseBody =
                Blob.valueOf('{ "Id" : "", "isSuccess" : "False", "Status" : "Failure",
                    "RuntimeError" : "'+e.getMessage()+'" }'); */
                resparse.StatusRec = 'failure';
                resparse.isSuccess = False;
                resparse.Error = 'Error'+e.getMessage();
            }
        return resparse;
    }


    //Deletes records.
    // Callout : /services/apexrest/Lead/<RecordId>
    @HttpDelete
    global static parse deleteLead() {
       // RestResponse res = RestContext.response;
        parse resparse = new parse();
        //HttpResponse response = new HttpResponse();
        String fullname;
        try{
           // String Errormessage; */
            //Lead thisLead;
            RestRequest request = RestContext.request;
            String leadId = request.requestURI.substring(
                request.requestURI.lastIndexOf('/')+1);
            Lead thisLead = [SELECT Id,LastName,FirstName,Name FROM Lead WHERE Id = :leadId];
            fullname = thisLead.FirstName+''+thisLead.LastName;
            delete thisLead;
            resparse.StatusRec = 'Success';
            resparse.isSuccess = True;
            resparse.recId = thisLead.Id;
            /*RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody =
            Blob.valueOf('{ "Name" : "'+thisLead.Name+'", "isSuccess" : "True",
                "Status" : "Success" }'); */
        }catch(Exception e){

            resparse.StatusRec = 'Failure';
            resparse.isSuccess = False;
            resparse.Error = 'Error:'+e.getMessage();
            resparse.recName = fullname;
        }
        return resparse;
    }


    //Typically used to update existing records or create records.
     /* Callout : /services/apexrest/Lead/
        JSON : {
                    "company" : "TTTTTTTT!",
                    "status" : "Open - Not Contacted",
                    "lastname" : "Medium",
                    "id": "00Q6F00001Cy1ph"

                }
    */
    @HttpPut
    global static parse upsertLead(String id,String company, String status,
        String firstname,String lastname,String phone,String email) {
        Lead thisLead;
        parse resparse = new parse();
        try{
            thisLead = new Lead(
                    Id=id,
                    Company=company,
                    Status=status,
                    FirstName=firstname,
                    LastName=lastname,
                    Phone=phone,
                    Email=email
                );
            // Match Lead by Id, if present.
            // Otherwise, create new Lead.
            upsert thisLead;
            resparse.StatusRec = 'Success';
            resparse.isSuccess = True;
            resparse.recId = thisLead.Id;
            // Return the Response in JSON.
            /*RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody =
            Blob.valueOf('{ "Id" : "'+thisLead.Id+'", "isSuccess" : "True",
                "Status" : "Success" }'); */
        }catch(Exception e){

               /* RestResponse res = RestContext.response;
                //e.getMessage()
                res.responseBody = Blob.valueOf('{"Id" : "","isSuccess" : "False" ,
                    "Error" : "Id Not Found", "SOQL Error" : "'+e.getMessage()+'"}'); */
            resparse.StatusRec = 'failure';
            resparse.isSuccess = False;
            resparse.Error = 'Error:'+e.getMessage();
        }
        return resparse;
    }



    //Typically used to update fields in existing records.
     /* Callout : /services/apexrest/Lead/<Record ID>
        JSON :  {
                    "Company" : "Patch",
                    "LastName" : "TestingPatch"
                }
    */
    @HttpPatch
    global static parse updateLeadFields() {
        Lead thisLead;
        parse resparse = new parse();
        try{
            RestRequest request = RestContext.request;
            String leadId = request.requestURI.substring(
                request.requestURI.lastIndexOf('/')+1);
            thisLead = [SELECT Id FROM Lead WHERE Id = :leadId];
            // Deserialize the JSON string into name-value pairs
            Map<String, Object> params =
                (Map<String, Object>)JSON.deserializeUntyped(request.requestbody.tostring());
            // Iterate through each parameter field and value
            for(String fieldName : params.keySet()) {
                // Set the field and value on the Lead sObject
                thisLead.put(fieldName, params.get(fieldName));
            }
            update thisLead;
            resparse.StatusRec = 'Success';
            resparse.isSuccess = True;
            resparse.recId = thisLead.Id;
           /* RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody =
            Blob.valueOf('{ "Id" : "'+thisLead.Id+'", "isSuccess" : "True",
                "Status" : "Success" }'); */
            }catch(Exception e){

                resparse.StatusRec = 'failure';
                resparse.isSuccess = False;
                resparse.Error = 'Error:'+e.getMessage();
               /* RestContext.response.addHeader('Content-Type', 'application/json');
                RestContext.response.responseBody =
                Blob.valueOf('{ "Id" : "", "isSuccess" : "False", "Status" : "Failure",
                    "RuntimeError" : "'+e.getMessage()+'" }'); */
        }
        return resparse;
    }
}