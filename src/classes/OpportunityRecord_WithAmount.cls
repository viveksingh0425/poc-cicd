/****************************************************************************************
 * Class : OpportunityRecord_WithAmount
 * Created By : Vivek Singh
 --------------------------------------------------------------------------------------------------------
 * Description : Write a SOQL query to display 100 opportunity records with amount greater than 10,000 order
 				 by created date. Skip first 50 records and include records from recycle bin.
 --------------------------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        16/07/2018       Initial Apex Learning
 ***************************************************************************************/

public with sharing class OpportunityRecord_WithAmount {
	public OpportunityRecord_WithAmount() {

		try{
			List<Opportunity> opt_record = [SELECT
											ID,Name
											FROM opportunity
											WHERE opportunity.amount > 10000
											ORDER BY createddate LIMIT 100 OFFSET 50 ALL ROWS];
                                            //Try with LIMIT 10 OFFSET 5 because data is not present
			System.debug(opt_record);
		}catch(DMLException ex)
		{
			System.debug('Error:'+ex);
		}
	}
}

/* test the following query and why error is occure ?
											SELECT
											ID,Name
											FROM opportunity
											WHERE opportunity.amount > 10000
											ORDER BY createddate LIMIT 100 OFFSET 50 ALL ROWS

SELECT ID,Name FROM opportunity WHERE opportunity.amount > 10000 ORDER BY createddate ALL ROWS  LIMIT 100 OFFSET 50
*/