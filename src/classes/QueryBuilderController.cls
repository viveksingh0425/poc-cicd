/***************************************************************************************************
 * ExtensionName : QueryBuilderController
 * Created By : Vivek Singh
 --------------------------------------------------------------------------------------------------
 * Description : This VisualForcePage provide Query Builder Application.
 * ProblemStatement : Develop a Query Bulder Application based on SOSL Query.
 * PageName: QueryBuilder
 --------------------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        15/08/2018          Dynamic Apex
 **************************************************************************************************/
public with sharing class QueryBuilderController {
    Public String selectedObject {get; set;} // object selected
    Public List<SelectOption> objFields {get;set;}
    Public List<String> selectedFields {get;set;} //fields selected
    Public String limitSOSL {get;set;} // SOSL limit
    Public String order {get;set;} // order by value
    Public String searchString {get;set;} //text to find
    Public String SOSLQuery {get; set;} //SOSL query to run
    Public List<sObject> SOSLResult {get;set;}
    Public List<string> lstFlds {get;set;}
    public QueryBuilderController() {
    }
     /**
     * Method will implement Schema-class Methods to get All fields
     *
     * @Param NULL
     *
     * @return List<SelectOption>-Options
     */
    public List<SelectOption> getObjectNames(){
        List<Schema.sObjectType> objectNames = Schema.getGlobalDescribe().Values();
        List<SelectOption> options = new List<SelectOption>();
        Map<String,String> sortedObj = new Map<String,String>();
        List<String> sortedObjList = new List<String>();
        options.add(new SelectOption ('--None--','--None--'));
        Integer num1, num2;
        num1=0;
        num2=0;
        for(Schema.sObjectType objName : objectNames){
            Schema.DescribeSObjectResult objResult = objName.getDescribe();
            num1++;
            if(objResult.isQueryable()){
                num2++;
                //system.debug(objResult.isQueryable());
                sortedObj.put(objName.getDescribe().getLabel(),objName.getDescribe().getName());
            }
        }
        sortedObjList.addAll(sortedObj.keyset());
        //System.debug(num1);
        //System.debug(num2);
        sortedObjList.sort();
        for(String sortKey : sortedObjList){
            options.add(new SelectOption(sortedObj.get(sortKey),sortKey));
        }
        return options;
    }
    /**
     * Method will Use Schema-class-Methods
     *
     * @Param NULL
     *
     * @return Nothing
     */
    public void getFields(){
        objFields = new List<SelectOption>();
        Map<String,Schema.sObjectType> allObjects = Schema.getGlobalDescribe();
        DescribeSObjectResult objDescribe = allObjects.get(selectedObject).getDescribe();
        if(objDescribe != null){
            Map<String,sObjectField> fieldMap = objDescribe.fields.getmap();
            List<String> sortedFields = new List<String>();
            for(String fname : fieldMap.keySet()){
                    sortedFields.add(fname);
            }
            sortedFields.sort();
            for(String sortedField : sortedFields){
                objFields.add(new SelectOption (fieldMap.get(sortedField).getDescribe().getName(),
                    sortedField));
                System.debug(sortedField);
            }
        }
    }
    /**
     * Method will Run SOSL Selected by User
     *
     * @Param NULL
     *
     * @return Nothing
     */
    public void runSOSL(){
            System.debug(selectedObject);
            System.debug(selectedFields);
            System.debug(limitSOSL);
            System.debug(order);
            System.debug(searchString);
            SOSLQuery = 'FIND \'' + searchString + '\' IN ALL FIELDS RETURNING ' + selectedObject + selectedFields+ 'ORDER BY' + selectedFields+' '+ order + 'LIMIT ' +limitSOSL;
         
        /*for(Integer i = 0;i< selectedFields.size();i++ ){
            if(i==(selectedFields.size() - 1) ){
               SOSLQuery = 'FIND \'' + searchString + '\' IN ALL FIELDS'+
                    'RETURNING ' + selectedObject + ' ' + selectedFields[i] + ' LIMIT ' +limitSOSL;
            }else{
            }
        }*/
            System.debug(SOSLQuery);
            List<List<sObject>> results = Search.query(SOSLQuery);
            SOSLResult = results[0];
            System.debug(results);
             Set<string> setFlds = new Set<String>();
            for(integer i=0;i<SOSLResult.size();i++){
                setFlds.addAll(SOSLResult[i].getPopulatedFieldsAsMap().keySet());
             }
            lstFlds = new List<String>();
            lstFlds.addAll(setFlds);
            system.debug(lstFlds);
    }
}