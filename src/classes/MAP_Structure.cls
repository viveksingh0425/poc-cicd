/****************************************************************************************
 * Class : Map_Structure
 * Created By : Vivek Singh
 --------------------------------------------------------------------------------------------
 * Description : Write a SOQL query to retrieve/print all active Users. Prepare a Map having
 				 User Id as key and User record as value
 --------------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        16/07/2018       Initial Apex Learning
 ***************************************************************************************/

public with sharing class MAP_Structure {
	public static void MAP_Structure_Test() {

		List<Account> Account_Record =[Select Id,Name from Account];
		Map<String,Id> Map_type1 = new Map<String,Id>();
		Map<Id,Account> Map_type2 = new Map<Id,Account>();
		for(Account ac : Account_Record)
		{
			Map_type1.put(ac.Name,ac.Id);
			Map_type2.put(ac.Id,ac);
		}
		System.debug(Map_type1);
		System.debug(Map_type2);
	}
}

/*    Accourding to Question

	List<Account> Account_Record =[Select Id,Name from Account];
	Map<String,Id> Map_type1 = new Map<String,Id>();
	Map<Id,String> Map_type2 = new Map<Id,String>();
	for(Account ac : Account_Record)
	{
		Map_type1.put(ac.Name,ac.Id);
		Map_type2.put(ac.Id,ac.Name);
	}
	System.debug(Map_type1);
	System.debug(Map_type2);

*/