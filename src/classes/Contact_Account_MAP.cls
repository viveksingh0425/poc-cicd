/****************************************************************************************
 * Class : Contact_Account__MAP
 * Created By : Vivek Singh
 -------------------------------------------------------------------------------------------
 * Description : Query on Contact and get its Account Id and Account object. Create a
				 Map which has key as Contact Id and Value as a whole Account object.
 -------------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        13/07/2018       Initial Apex Learning
 ***************************************************************************************/

public with sharing class Contact_Account_MAP {
	public Contact_Account_MAP() {

		try
		{
			List<Contact> contactList = [SELECT
								AccountId,
								id,
								Name
								FROM Contact];
			List<Account> accountList = [SELECT
										id
										FROM Account];
			Map<ID,Account> contactAccountMap = new Map<ID,Account>();//MAP of Type ID and Account-Object
			for(Integer i=0;i<contactList.size();i++){ //Loop for Contact Traversal
			//Loop for Account Traversal to check whether the Contact is associated with any of the account or not.
				for(Integer j=0;j<accountList.size();j++){
						if(contactList[i].AccountId == accountList[j].id)
							contactAccountMap.put(contactList[i].id,accountList[j]);
				}
			}
			System.debug(''+contactList);
			System.debug(''+contactList.size());
			System.debug(''+accountList);
			System.debug(''+accountList.size());
			System.debug(''+contactAccountMap);
			System.debug(''+contactAccountMap.size());
		}catch(DMLException ex){
			System.debug('Error:'+ex);
		}
	}
}