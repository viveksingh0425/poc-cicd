/**************************************************************************************************
 * Scheduler Class : RecordCreationEngineScheduler
 * Created By : Vivek Singh
 --------------------------------------------------------------------------------------------------
 * Description : Schedular for calling  a job in each 3 hours
 * Test Class : DeleteDuplicateLeadTest
 * Class : DeleteDuplicateLead
 --------------------------------------------------------------------------------------------------
 * Version History:
 * Version  Developer Name    Date          Detail Features
 * 1.0      Vivek Singh     08/03/2018    Initial Development
 **************************************************************************************************/

global class DeleteDuplicateLeadScheduler implements Schedulable{

    global void execute(SchedulableContext sc){
        ID BatchId = Database.executeBatch(new DeleteDuplicateLead(), 2);
        System.debug('Batch ID for Chunk'+BatchId);
    }
}

/* RUN Following Method in Anonymous Window of Salesforce
        DeleteDuplicateLeadScheduler batchSchedulerObj = new DeleteDuplicateLeadScheduler();
        /**
            CronJobInterval : ',HH,HH+3,HH+6,......'
         */
    /*
        String CronJobInterval = '0 0 0 3,6,12,15,18,21 * * ?';
        String jobID = system.schedule('DeleteDuplicateLead In Each 3hrs', CronJobInterval, batchSchedulerObj);
*/