/**************************************************************************************************
* Class : WetherController
* Created By : Vivek Singh
--------------------------------------------------------------------------------------------------
* Description : Fetch the Weather Historical Data
* Test Class : WeatherControllerTest
--------------------------------------------------------------------------------------------------
* Version History:
* Version  Developer Name    Date          Detail Features
* 1.0      Vivek Singh       03/01/2019    Initial Development
**************************************************************************************************/

public class WeatherController{
    public  List<Document> 			docsList;
    public  boolean 				showResult			{get;set;}
    public  boolean 				blnNoResultFound	{get;set;}
    public  List<WeatherWrapper> 	listOfWeatherWraper	{get;set;}
    private integer 				count = 1;   //to track the function calling
    private integer 				counter = 0;
    private integer 				list_size = 10;
    public  integer 				total_size;
    public  String 					weatherId 			{get;set;}
    public Weather_History__c 		objWeatherHistory	{get;set;}
    public String					temp;
    public String					pressure;
    public String					humidity;
    public String					temp_min;
    public String					temp_max;
    public String					weatherdate;
    
    public WeatherController(ApexPages.StandardController controller) {
        weatherId  = ApexPages.CurrentPage().getparameters().get('id');
        objWeatherHistory = (Weather_History__c) controller.getRecord();
        docsList = new List<Document>();
        showResult = false;
        listOfWeatherWraper = new List<WeatherWrapper>();
    }
    
    /**
    * Method will get weather hitorical data.
    * @return weather wrapper containing all weather info.
    */
    
    public PageReference searchDateRange() {
        showResult = false;
        listOfWeatherWraper = new List<WeatherWrapper>();
        
        if(objWeatherHistory.City_Name__c == NULL || objWeatherHistory.Start_Date__c == NULL 
           			|| objWeatherHistory.End_Date__c == NULL) {
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Error,'Please enter all the details.');
            ApexPages.addMessage(errorMsg);
            return NULL;
        }
        
        List<Weather_History__c> lstWeatherHistory = new List<Weather_History__c>();
        lstWeatherHistory = [ SELECT id
                             , Start_Date__c
                             , End_Date__c
                             , City_Name__c
                             , Temperature__c
                             , Pressure__c
                             , Humidity__c
                             , Minimum_Temperature__c
                             , Maximum_Temperature__c
                             FROM Weather_History__c
                             WHERE City_Name__c = :objWeatherHistory.City_Name__c
                             AND (Start_Date__c >= :objWeatherHistory.Start_Date__c
                                  AND Start_Date__c <= :objWeatherHistory.End_Date__c)
                            ];
        
        if(lstWeatherHistory.size() > 0) {
            for(Weather_History__c objWeather : lstWeatherHistory) {
                WeatherWrapper objWeatherWrapper = new WeatherWrapper(String.valueOf(objWeather.Start_Date__c)
                                                                      ,objWeather.Temperature__c
                                                                      ,objWeather.Pressure__c
                                                                      ,objWeather.Humidity__c
                                                                      ,objWeather.Minimum_Temperature__c
                                                                      ,objWeather.Maximum_Temperature__c);
                listOfWeatherWraper.add(objWeatherWrapper);
            }
        }
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('http://api.openweathermap.org/data/2.5/weather?q='
							+objWeatherHistory.City_Name__c+'&units=metric&appid=22cb465d835d3d9e292b39f22b4685af');
        request.setMethod('GET');
        
        HttpResponse response = http.send(request);
        System.debug('Status Code : '+response.getStatusCode());
        
        if (response.getStatusCode() == 200 ) { 
            Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(response.getBody()); 
            for(String str : results.keyset()) {
                if(str == 'main') {
                    Map<String, Object> mainResult = (Map<String, Object>)(results.get(str));
                    
                    weatherdate = String.valueOf(objWeatherHistory.Start_Date__c);
                    temp = String.valueOf(mainResult.get('temp'));
                    pressure = String.valueOf(mainResult.get('pressure'));
                    humidity = String.valueOf(mainResult.get('humidity'));
                    temp_min = String.valueOf(mainResult.get('temp_min'));
                    temp_max = String.valueOf(mainResult.get('temp_max'));
                    
                    WeatherWrapper objWeatherWrapper = new WeatherWrapper(weatherdate,temp,pressure,humidity,temp_min,temp_max);
                    listOfWeatherWraper.add(objWeatherWrapper);
                }
            }
        }
        
        if(listOfWeatherWraper.size() > 0) {
            showResult = true;            
        }
        
        return NULL; 
    }
    
    /**
    * Method will save records to the database.
    */
    
    public PageReference saveRecord() {
        List<Weather_History__c> lstWeatherHistory = new List<Weather_History__c>();
        
        for(WeatherWrapper objWeatherWrapper : listOfWeatherWraper) {
            Weather_History__c objWeather = new Weather_History__c();
            objWeather.City_Name__c = objWeatherHistory.City_Name__c;
            objWeather.Start_Date__c = objWeatherHistory.Start_Date__c;
            objWeather.End_Date__c = objWeatherHistory.End_Date__c;
            objWeather.Temperature__c = objWeatherWrapper.temp;
            objWeather.Pressure__c = objWeatherWrapper.pressure;
            objWeather.Humidity__c = objWeatherWrapper.humidity;
            objWeather.Minimum_Temperature__c = objWeatherWrapper.temp_min;
            objWeather.Maximum_Temperature__c = objWeatherWrapper.temp_max;
            lstWeatherHistory.add(objWeather);
        }
        if(lstWeatherHistory.size() > 0) {
            insert lstWeatherHistory;
        }
        ApexPages.Message successMsg = new ApexPages.Message(ApexPages.Severity.Info,'Record has been saved successfully');
        ApexPages.addMessage(successMsg);
        return NULL;
    }
    
    /**
    * Method will redirect to the pdf of the records
    * @return PageReference for the pdf
    */
    
    public PageReference exportToPdf() {
        String pdfLink = '/apex/WeatherPdf?city='+objWeatherHistory.City_Name__c
            			+'&start='+objWeatherHistory.Start_Date__c
            			+'&end='+objWeatherHistory.End_Date__c;
        PageReference pdfPage = new PageReference(pdfLink);
        pdfPage.setRedirect(true);
        return pdfPage ;
    }
    
    //Wrapper class to deserialize json data
    public class WeatherWrapper {
        public String weatherDate 	{get;set;}
        public String temp 			{get;set;}
        public String pressure 		{get;set;}
        public String humidity 		{get;set;}
        public String temp_min 		{get;set;}
        public String temp_max 		{get;set;}
        
        public WeatherWrapper(String weatherDate, String temp, String pressure, String humidity, String temp_min, String temp_max) {
            this.weatherDate = weatherDate;
            this.temp = temp;
            this.pressure = pressure;
            this.humidity = humidity;
            this.temp_min = temp_min;
            this.temp_max = temp_max;
        } 
    }
}