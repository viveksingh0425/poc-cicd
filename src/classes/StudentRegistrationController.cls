public with sharing class StudentRegistrationController {


    public StudentRegistrationController(){
        selectedLang='en';

        listOfLang = new List<selectOption>();
        listOfLang.add(new selectOption('en','English'));
        listOfLang.add(new selectOption('es','Spanish'));
        listOfLang.add(new selectOption('fr','French'));
    }
    public PageReference doInsert() {
        Student__c stud = new Student__c();
        stud.name=nameVal;
        stud.city__c=cityVal;
        stud.country__c=countryVal;
        stud.state__c=stateVal;
        stud.S_S_C__c=sscVal;
        stud.H_S_C__c=hscVal;
        stud.Roll_no__c=rollnoVal;
        stud.Gender__c=gender;
        stud.College__c='testing';
        insert stud;
        pagereference ref = new pagereference('/apex/messsagePage');
        ref.setredirect(true);
        return ref;
    }
    public Student__c stud{get;set;}
    public Decimal sscVal { get; set; }
    public Integer rollnoVal { get; set; }
    public Decimal hscVal { get; set; }
    public String gender { get; set; }
    public String nameVal { get; set; }
    public String countryVal { get; set; }
    public String cityVal { get; set; }
    public String stateVal { get; set; }
    public string selectedLang{get;set;}
    public List<selectoption> listOfLang {get;set;}

}