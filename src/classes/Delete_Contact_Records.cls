/****************************************************************************************
 * Class : Delete_Contact_Records
 * Created By : Vivek Singh
 -------------------------------------------------------------------------------------------
 * Description : Delete all contact records.
 -------------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        13/07/2018       Initial Apex Learning
 ***************************************************************************************/

public with sharing class Delete_Contact_Records {
	public Delete_Contact_Records() {

		try
		{
			List<contact> con =[Select ID from contact];
			Database.delete(con,false);
		}catch(DMLException ex){
			System.debug('Error:'+ex);
		}
	}
}