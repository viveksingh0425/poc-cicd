/****************************************************************************************
 * Class : Opertunity_Groupby
 * Created By : Vivek Singh
 --------------------------------------------------------------------------------------------------------
 * Description : Write a SOQL query to retrieve sum of all closed Opportunity amount for current fiscal year.
 				 Store this information in a map with key as year and value as sum of amount. Iterate this map
				 to display statistics
 --------------------------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        16/07/2018       Initial Apex Learning
 ***************************************************************************************/

public with sharing class Opertunity_Groupby {
	public Opertunity_Groupby() {

		List<AggregateResult> Opt_Record = [SELECT
											CALENDAR_YEAR(CloseDate),SUM(Amount)
											FROM Opportunity WHERE StageName = 'Closed Lost'
											GROUP BY CloseDate];
		Map<Object,Object> map_yr_Opportunity = new Map<Object,Object>();
		for (AggregateResult ar : Opt_Record)  {
			map_yr_Opportunity.put(ar.get('expr0'),ar.get('expr1'));
			System.debug('CALENDAR_YEAR=' + ar.get('expr0') +'\tSUM of amount=' + ar.get('expr1'));
		}
		System.debug(map_yr_Opportunity);

	}
}