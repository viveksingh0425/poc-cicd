/***************************************************************************************************
 * Class : DatafactoryClass For trigger Testing
 * Created By : Vivek Singh
 ---------------------------------------------------------------------------------------------------
 * Description : This Class have many datafactory Record creation methods for testing
 ---------------------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        23/07/2018       Initial Apex Trigger Learning
 **************************************************************************************************/

@isTest
public with sharing class testDataFactoryTrigger{
	//Create test Data for Positive Account
     public void createAcRecPositive(Integer numAccts){
        List<Account> accts = new List<Account>();
        for(Integer i=0;i<numAccts;i++) {
            Account a = new Account(Name='TestJohn' + i,website = 'vivek@code.com',
                                    Ac_email__c ='vivek@gmail.com');
            accts.add(a);
        }
        insert accts;
     }
     //Create test Data for Negative Account
     public void createAcRecNegative(Integer numAccts){
        List<Account> accts = new List<Account>();
        for(Integer i=0;i<numAccts;i++) {
            Account a = new Account(Name='TestJohn' + i,website = 'vivek@gml.com');
            accts.add(a);
        }
        insert accts;
     }
     //Create test Data for Positive Contact
      public void createConRecPositive(Integer numCons){
        List<Contact> con = new List<Contact>();
        for(Integer i=0;i<numCons;i++) {
            Contact a = new Contact(LastName='TestJohn' + i,Email = 'vivek@gmail.com');
            con.add(a);
        }
        insert con;
     }
      //Create test Data for Negative Contact
     public void createConRecNegative(Integer numCons){
        List<Contact> con = new List<Contact>();
        for(Integer i=0;i<numCons;i++) {
            Contact a = new Contact(LastName='TestJohn' + i,Email = 'vivek@gml.com');
            con.add(a);
        }
        insert con;
     }
     //Create test Data for Positive Case
      public void createcaseRecPositive(Integer numcase){
        List<case> cse = new List<case>();
        for(Integer i=0;i<numcase;i++) {
            case a = new case(Status = 'New', Origin = 'Email',Case_Email__c = 'vivek@gmail.com');
            cse.add(a);
        }
        insert cse;
     }
     //Create test Data for Negative Case
      public void createcaseRecNegative(Integer numcase){
        List<case> cse = new List<case>();
        for(Integer i=0;i<numcase;i++) {
            case a = new case(Status = 'Working', Origin = 'Web',Case_Email__c = 'vivek@gml.com');
            cse.add(a);
        }
        insert cse;
     }
}