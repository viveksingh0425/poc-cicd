/**************************************************************************************************
 * Class : RecordCreationEngine
 * Created By : Vivek Singh
 --------------------------------------------------------------------------------------------------
 * Description : RecordCreationEngine using JSON Parsing and Custome.
 * Test Class : RecordCreationEngineTest
 * Scheduler Class : RecordCreationEngineScheduler
 --------------------------------------------------------------------------------------------------
 * Version History:
 * Version  Developer Name    Date          Detail Features
 * 1.0      Vivek Singh     08/03/2018    Initial Development
 **************************************************************************************************/

global class RecordCreationEngine implements Database.Batchable<sObject>{

     global RecordCreationEngine(){
                // Batch Constructor
     }

     // Start Method
     global Database.QueryLocator start(Database.BatchableContext BC){
     String Query = ''; // Generate your string query on any object here with limit 10000
      return Database.getQueryLocator(Query); //Query is Required on object which you want to run Batch
     }

   // Execute Logic
    global void execute(Database.BatchableContext BC, List<sObject> objlist){

        String jSONSObject = '{"LastName":"abc"}';
        String sObjectApiName = 'Contact';
        Map<String, Object> fieldMap = (Map<String, Object>)JSON.deserializeUntyped(jSONSObject);
        System.debug(fieldMap);
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(sObjectApiName);
        System.debug(sObjectApiName);
        SObject newSobject = targetType.newSObject();
        System.debug(newSobject);
        Map<String, Schema.sObjectField> targetFields = targetType.getDescribe().fields.getMap();
        System.debug(targetFields);
        for (String key : fieldMap.keySet())
        {
            Object value = fieldMap.get(key);
            Schema.DisplayType valueType = targetFields.get(key).getDescribe().getType();
            System.debug(valueType);
            if (value instanceof String && valueType != Schema.DisplayType.String)
            {
                String svalue = (String)value;
                if (valueType == Schema.DisplayType.Date)
                    newSobject.put(key, Date.valueOf(svalue));
                else if(valueType == Schema.DisplayType.DateTime)
                    newSobject.put(key, DateTime.valueOfGmt(svalue));
                else if (valueType == Schema.DisplayType.Percent || valueType == Schema.DisplayType.Currency)
                    newSobject.put(key, svalue == '' ? null : Decimal.valueOf(svalue));
                else if (valueType == Schema.DisplayType.Double)
                    newSobject.put(key, svalue == '' ? null : Double.valueOf(svalue));
                else if (valueType == Schema.DisplayType.Integer)
                    newSobject.put(key, Integer.valueOf(svalue));
                else if (valueType == Schema.DisplayType.Base64)
                    newSobject.put(key, Blob.valueOf(svalue));
                else
                    newSobject.put(key, svalue);
            }
            else
                newSobject.put(key, value);
        }
        insert newSobject;
    }
    global void finish(Database.BatchableContext BC){
        // Logic to be Executed at finish
    }
 }