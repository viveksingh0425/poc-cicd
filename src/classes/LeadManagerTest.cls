/**************************************************************************************************
 * Test Class : LeadManager
 * Created By : Vivek Singh
 --------------------------------------------------------------------------------------------------
 * Description : Lead Manipulator Service Using REST.
 * Class : LeadManager
 --------------------------------------------------------------------------------------------------
 * Version History:
 * Version  Developer Name    Date          Detail Features
 * 1.0      Vivek Singh     08/07/2018    Initial WebService Development
 **************************************************************************************************/

@IsTest
private class LeadManagerTest {
    @isTest static void testGetLeadById() {
        Id recordId = createTestRecord();
        // Set up a test request
        RestRequest request = new RestRequest();
        request.requestUri =
            'https://login.salesforce.com/services/apexrest/Lead/'
            + recordId;
        request.httpMethod = 'GET';
        RestContext.request = request;
        LeadManager.parse res = new LeadManager.parse();
        // Call the method to test
        res = LeadManager.getLeadById();
        // Verify results
        System.debug(res);
        System.assert(res != null);
        System.assertEquals(True, res.isSuccess);
       // System.assertEquals('Success', res.StatusRec);
    }
    @isTest static void testCreateLead() {
        // Call the method to test
       LeadManager.createLead('TestClass', 'Open-Not Contacted', 'TestFirstName','TestLastName',
                                    '2445567788','viveeeeek@gmail.com');
        // Verify results
        List<Lead> thisLeadls = [SELECT
                                        Id
                                    FROM
                                        Lead
                                    WHERE
                                        Company = 'TestClass'];
        ID thisLeadId;
        for(Lead ld : thisLeadls){
                thisLeadId = ld.Id;
        }
        System.debug('LeadcreationID'+thisLeadId);

        System.assert(thisLeadId != null);
        Lead thisLead = [SELECT
                                Id,
                                Company,
                                Status
                            FROM
                                Lead
                            WHERE
                                Id=:thisLeadId];
        System.debug('Createee'+thisLead);
        System.assert(thisLead != null);
        System.assertEquals(thisLead.Company, 'TestClass');
    }
    @isTest static void testDeleteLead() {
        Id recordId = createTestRecord();
         System.debug('DeleteIDDD'+recordId);
        // Set up a test request
        RestRequest request = new RestRequest();
        request.requestUri =
            'https://login.salesforce.com/services/apexrest/Lead/'
            + recordId;
        request.httpMethod = 'GET';
        RestContext.request = request;
        // Call the method to test
        LeadManager.deleteLead();
        // Verify record is deleted
        List<Lead> Leads = [SELECT
                                    Id
                                FROM
                                    Lead
                                WHERE
                                    Id=:recordId];
        System.debug('RECORDDDD'+Leads);
        System.assert(Leads.size() == 0);
    }
    @isTest static void testUpsertLead() {
        // 1. Insert new record
        LeadManager.parse res = new LeadManager.parse();
        res = LeadManager.upsertLead(null,'TestClass1', 'Open-Not Contacted',
                                'TestFirstName','TestLastName','2445567788','viveeeeek@gmail.com');
        // Verify new record was created
       // System.assert(Lead1Id != null);
        System.debug(res);
        List<Lead> Lead1 = [SELECT
                                    Id,
                                    Company
                                FROM
                                    Lead
                                WHERE
                                    id =: res.recId];
        System.debug(Lead1);
        ID Lead1Id;
        String Cname;
        for(Lead ld : Lead1){
                Lead1Id = ld.Id;
                Cname = ld.Company;
        }
        System.debug(Lead1Id);
        System.assert(Lead1 != null);
        System.assertEquals(Cname, 'TestClass1');
        // 2. Update status of existing record to Working
        LeadManager.upsertLead(Lead1Id,'TestClass2', 'Open-Not Contacted',
                'TestFirstNameup','TestLastNamepp','2445567788','viveeeeek@gmail.com');
        // Verify record was updated
        ID Lead2Id;
        String statusld2;
        List<Lead> ldrec = [Select Id,Status from Lead Where Company = 'TestClass2'];
        for(Lead ld : ldrec){
                Lead2Id = ld.Id;
                statusld2 = ld.status;
        }
        System.assertEquals(Lead1Id, Lead2Id);
        Lead Lead2 = [SELECT
                        Id,Status
                    FROM
                        Lead WHERE Id=:Lead2Id];
        System.assert(Lead2 != null);
        System.assertEquals(Lead2.Status, 'Open-Not Contacted');
    }
    @isTest static void testUpdateLeadFields() {
        Id recordId = createTestRecord();
        RestRequest request = new RestRequest();
        request.requestUri =
            'https://login.salesforce.com/services/apexrest/Lead/'
            + recordId;
        request.httpMethod = 'PATCH';
        request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueOf('{"status": "Open-Not Contacted","Phone": "123456","Lastname": "HHH"}');
        RestContext.request = request;
        // Update status of existing record to Working
        LeadManager.updateLeadFields();
        // Verify record was updated
        List<Lead> lstrec = [SELECT Id from Lead where Lastname = 'HHH'];
        ID thisLeadId;
        for(Lead ld : lstrec){
            thisLeadId = ld.Id;
        }
        System.assert(thisLeadId != null);
        Lead thisLead = [SELECT Id,Status FROM Lead WHERE Id=:thisLeadId];
        System.assert(thisLead != null);
        System.assertEquals(thisLead.Status, 'Open-Not Contacted');
    }
    // Helper method
    static Id createTestRecord() {
        // Create test record
        Lead LeadTest = new Lead(
            Company='Test12',
            Status='Open-Not Contacted',
            FirstName='rameshh122',
            Lastname = 'Medium11',
            Phone = '123456781',
            Email = 'vive11k@gmail.com'
            );
        insert LeadTest;
        return LeadTest.Id;
    }
}