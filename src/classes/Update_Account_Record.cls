/****************************************************************************************
 * Class : Update_Account_Records
 * Created By : Vivek Singh
 -------------------------------------------------------------------------------------------
 * Description : Query all Account records and update its name to “Eternus”.
 -------------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        13/07/2018       Initial Apex Learning
 ***************************************************************************************/

public with sharing class Update_Account_Record {
	public Update_Account_Record() {

		try
		{
			List<Account> ac = [Select ID from Account];
			for(Account a : ac)
			{
				a.Name = 'Eternus_TEST';
			}
			Database.update(ac,false);
		}catch(DMLException ex){
			System.debug('Error'+ex);
		}
	}
}