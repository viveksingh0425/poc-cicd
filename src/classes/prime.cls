/****************************************************************************************
 * Class : Prime
 * Created By : Vivek Singh
 ----------------------------------------------------------------------------------------
 * Description : Checking whether the number is prime or not
 ----------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        12/07/2018       Initial Apex Learning
 ***************************************************************************************/

public with sharing class prime {

    public static Boolean prime(integer num){
    for(integer i = 2; i < num; i++)
    {
        if(math.mod(num,i) == 0)
        {
            System.debug(num + ' is not prime');
            return false;
        }
    }
    System.debug(num + ' is prime');
    return true;
    }
}