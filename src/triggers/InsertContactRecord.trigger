/***************************************************************************************************
 * Class : TestClass for InsertContactTrigger
 * Created By : Vivek Singh
 ---------------------------------------------------------------------------------------------------
 * Description : Trigger for Updating Cases that come in from the Web, Email or Phone
    Use Case – we would like to use the email address of the incoming case to see if we can associate the
    correct account AND/OR contact to populate the Account and Contact Fields.
    When a new case is created and the Case.Origin field is set to “Phone” or “Email” or “Web” take the
    Case.Email__c field and look up to find a match in the following account field –
    Account.Email_Address__c(Custome Field) And Conatct field - Contact.Email
 ---------------------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   2.0         Vivek Singh        23/07/2018       Initial Apex Trigger Learning
 **************************************************************************************************/

trigger InsertContactRecord on Contact (before insert,before update) {


  /*  Map<String, Id> domains = new Map<String, Id>();
    for(Contact record: Trigger.new) {
        domains.put(record.Email_domain__c, null);
    }
    for(Account record: [SELECT
                             Website_Domain__c
                        FROM Account
                        WHERE
                             Website_Domain__c IN :domains.keySet()]) {
        domains.put(record.Website_Domain__c, record.Id);
    }
    for(Contact record: Trigger.new) {
        if(domains.get(record.Email_domain__c) != null) {
            record.AccountId = domains.get(record.Email_domain__c);
        } // you can figure out an else here, if you want
    } */

    if((Trigger.isinsert || Trigger.isupdate) && Trigger.isbefore)
    {
        ContactHandler.ContactHandlermethod(Trigger.New);
    }

}