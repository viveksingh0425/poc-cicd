/****************************************************************************************
 * Class : Clone_Apex_Trigger
 * Created By : Vivek Singh
 -------------------------------------------------------------------------------------------
 * Description : Write a Trigger on Lead which will create the clone record.
 -------------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        16/07/2018       Initial Apex Trigger Learning
 ***************************************************************************************/

trigger Clone_Apex_trigger on Lead (before insert,after insert) {
   if(trigger.isInsert && trigger.isBefore)
    {
        System.debug('Lead Before insert + NEW VALUE    '+trigger.NEW);
    }
    if(trigger.isInsert && trigger.isAfter){
           System.debug('Lead After insert'+trigger.NEW);
           if(helper_Clone_Apex_trigger.runOnce())
           {
             List<Lead> leadList = new List<Lead>();
             leadList = Trigger.new.deepClone();
             System.debug(leadList);
             insert leadList;
           }
    }
}

/*
    Lead myLead = new Lead();
    myLead.FirstName = 'Ram';
    myLead.LastName  = 'Ji';
    myLead.Company   = 'Unemployed';
    insert myLead;
*/