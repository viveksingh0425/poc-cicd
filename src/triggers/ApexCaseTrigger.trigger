/***************************************************************************************************
 * Class : ApexCaseTrigger
 * Created By : Vivek Singh
 ---------------------------------------------------------------------------------------------------
 * Description :Trigger for Updating Cases that come in from the Web, Email or Phone
                Use Case – we would like to use the email address of the incoming case to see if we
                can associate the correct account AND/OR contact to populate the Account and Contact
                Fields.
                When a new case is created and the Case.Origin field is set to “Phone” or “Email” or
                “Web” take the
                Case.Email__c field and look up to find a match in the following account field –
                Account.Email_Address__c(Custome Field) And Conatct field - Contact.Email
 ---------------------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        19/07/2018       Initial Apex Trigger Learning
 ***************************************************************************************/



trigger ApexCaseTrigger on Case (before insert,before update) {

    if((Trigger.isinsert || Trigger.isupdate) && Trigger.isbefore)
    {
        CaseHandler.casehandlermethod(Trigger.New);
        CaseHandler.casehandlermethodtest();
    }
}