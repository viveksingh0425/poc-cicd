/****************************************************************************************
 * Class : Apex_trigger_test
 * Created By : Vivek Singh
 -------------------------------------------------------------------------------------------
 * Description : Task Creation Trigger.
 -------------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         Vivek Singh        16/07/2018       Initial Apex Trigger Learning
   
  
 ***************************************************************************************/
 trigger Apex_trigger_test on Account (before insert,after insert,before update,after update,before delete) {
    if(trigger.isInsert && trigger.isBefore)
    {
        System.debug('Account Before insert + NEW VALUE '+trigger.NEW);
        System.debug('Account Before insert + NEWMAP VALUE  '+trigger.newMAP);
    }
    if((trigger.isInsert || trigger.isUpdate) && trigger.isAfter){
        LIST<Task> tasks = new LIST<Task>();
        list<Account> Account_list = [select id,Name from Account where id IN:Trigger.NEW];
        System.debug(Trigger.NEW);
        for(Account ac_obj:Account_list)
        {
            System.debug(Trigger.NewMAP.get(ac_obj.id).id);
            String Nme = ac_obj.Name;
            tasks.add(new Task(whatID = Trigger.NewMAP.get(ac_obj.id).id, Subject='Meeting with\t'+Nme));
        }
        Database.Saveresult[] res = database.insert(tasks,false);
            for (Database.SaveResult sr : res) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully inserted account. Account ID: ' + sr.getId());
                }
                else {
                    // Operation failed, so get all errors
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Account fields that affected this error: ' + err.getFields());
                    }
                }
            }
    }
    if(trigger.isInsert && trigger.isAfter)
    {
        System.debug('Account after insert + OLD VALUE'+trigger.OLD);
        System.debug('Account after insert + OLDMAP VALUE   '+trigger.oldMap);
    }
    if(trigger.isupdate && trigger.isbefore)
    {
        
        System.debug('Account before Update + NEW VALUE'+Trigger.New);
       
        System.debug('Account after update +  OLD VALUE'+Trigger.OLD);
        for(Account ac : Trigger.New){
           //System.debug('Account after Update + OLDMAP VALUE    '+trigger.oldmap.get(ac.name)); 
           System.debug('Account Before Update + newmap VALUE   '+trigger.newmap.get(ac.name));
        }
        
    }
    if(trigger.isupdate && trigger.isAfter)
    {
      //  System.debug('Account before Update + NEW VALUE'+Trigger.New);
       // System.debug('Account Before Update + OLDMAP VALUE  '+trigger.newmap);
       // System.debug('Account after update +  OLD VALUE'+Trigger.OLD);
        System.debug('Account after Update + OLDMAP VALUE   '+trigger.oldmap);
        
    }
    if(trigger.isdelete){
        System.debug('Account before delete+ OLD VALUE'+Trigger.OLD);
        System.debug('Account after delete + OLDMAP VALUE   '+trigger.oldmap);
        
    }
      
}